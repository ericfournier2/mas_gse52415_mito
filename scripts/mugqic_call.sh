export RAP_ID=def-masirard
module load mugqic/mugqic_pipelines


mkdir -p output/pipeline
/home/efournie/genpipes/pipelines/rnaseq/rnaseq.py -j slurm -s '1-22' -l debug \
    -r raw/readset.txt \
    -d raw/design.txt \
    -o output/pipeline \
    --config /home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini \
        /home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini \
        $MUGQIC_INSTALL_HOME/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini
