module load sra-toolkit

# Dump all fastq files.
for i in raw/*.sra
do
    fastq-dump --gzip $i
done

# Remove SRA files.
#rm raw/*.sra

# Remove SRA database file
#rm SRAmetadb.sqlite