#!/bin/bash
# Exit immediately on error
set -eu -o pipefail

#-------------------------------------------------------------------------------
# RnaSeq SlurmScheduler Job Submission Bash script
# Version: 3.0.1-beta
# Created on: 2018-08-19T12:59:36
# Steps:
#   picard_sam_to_fastq: 0 job... skipping
#   trimmomatic: 0 job... skipping
#   merge_trimmomatic_stats: 0 job... skipping
#   star: 7 jobs
#   picard_merge_sam_files: 0 job... skipping
#   picard_sort_sam: 18 jobs
#   picard_mark_duplicates: 18 jobs
#   picard_rna_metrics: 18 jobs
#   estimate_ribosomal_rna: 18 jobs
#   bam_hard_clip: 18 jobs
#   rnaseqc: 2 jobs
#   wiggle: 36 jobs
#   raw_counts: 18 jobs
#   raw_counts_metrics: 4 jobs
#   cufflinks: 18 jobs
#   cuffmerge: 1 job
#   cuffquant: 18 jobs
#   cuffdiff: 10 jobs
#   cuffnorm: 1 job
#   fpkm_correlation_matrix: 2 jobs
#   gq_seq_utils_exploratory_analysis_rnaseq: 3 jobs
#   differential_expression: 1 job
#   TOTAL: 211 jobs
#-------------------------------------------------------------------------------

OUTPUT_DIR=/project/6009125/GSE52415_mito/output/pipeline
JOB_OUTPUT_DIR=$OUTPUT_DIR/job_output
TIMESTAMP=`date +%FT%H.%M.%S`
JOB_LIST=$JOB_OUTPUT_DIR/RnaSeq_job_list_$TIMESTAMP
export CONFIG_FILES="/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini"
mkdir -p $OUTPUT_DIR
cd $OUTPUT_DIR


#-------------------------------------------------------------------------------
# STEP: star
#-------------------------------------------------------------------------------
STEP=star
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: star_1_JOB_ID: star_align.2.SRX377953
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.SRX377953
JOB_DEPENDENCIES=
JOB_DONE=job_output/star/star_align.2.SRX377953.fd151ebba5e55161e24a1e3e4e14173d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.SRX377953.fd151ebba5e55161e24a1e3e4e14173d.mugqic.done'
module load mugqic/star/2.5.3a && \
mkdir -p alignment/16-cell_rep1/SRX377953 && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/16-cell_rep1/SRX377953.trim.single.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/16-cell_rep1/SRX377953/ \
  --outSAMattrRGline ID:"SRX377953" 	PL:"ILLUMINA" 			SM:"16-cell_rep1" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 100000000000 \
  --limitBAMsortRAM 100000000000 \
  --limitIObufferSize 4000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f SRX377953/Aligned.sortedByCoord.out.bam alignment/16-cell_rep1/16-cell_rep1.sorted.bam
star_align.2.SRX377953.fd151ebba5e55161e24a1e3e4e14173d.mugqic.done
)
star_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=128G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$star_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: star_2_JOB_ID: star_align.2.SRX377954
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.SRX377954
JOB_DEPENDENCIES=
JOB_DONE=job_output/star/star_align.2.SRX377954.3516b56ad857721867abdc87df59a507.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.SRX377954.3516b56ad857721867abdc87df59a507.mugqic.done'
module load mugqic/star/2.5.3a && \
mkdir -p alignment/16-cell_rep2/SRX377954 && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/16-cell_rep2/SRX377954.trim.single.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/16-cell_rep2/SRX377954/ \
  --outSAMattrRGline ID:"SRX377954" 	PL:"ILLUMINA" 			SM:"16-cell_rep2" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 100000000000 \
  --limitBAMsortRAM 100000000000 \
  --limitIObufferSize 4000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f SRX377954/Aligned.sortedByCoord.out.bam alignment/16-cell_rep2/16-cell_rep2.sorted.bam
star_align.2.SRX377954.3516b56ad857721867abdc87df59a507.mugqic.done
)
star_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=128G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$star_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: star_3_JOB_ID: star_align.2.SRX377955
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.SRX377955
JOB_DEPENDENCIES=
JOB_DONE=job_output/star/star_align.2.SRX377955.f968d3145d2d789791cba2856c99dcf1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.SRX377955.f968d3145d2d789791cba2856c99dcf1.mugqic.done'
module load mugqic/star/2.5.3a && \
mkdir -p alignment/16-cell_rep3/SRX377955 && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/16-cell_rep3/SRX377955.trim.single.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/16-cell_rep3/SRX377955/ \
  --outSAMattrRGline ID:"SRX377955" 	PL:"ILLUMINA" 			SM:"16-cell_rep3" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 100000000000 \
  --limitBAMsortRAM 100000000000 \
  --limitIObufferSize 4000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f SRX377955/Aligned.sortedByCoord.out.bam alignment/16-cell_rep3/16-cell_rep3.sorted.bam
star_align.2.SRX377955.f968d3145d2d789791cba2856c99dcf1.mugqic.done
)
star_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=128G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$star_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: star_4_JOB_ID: star_align.2.SRX377956
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.SRX377956
JOB_DEPENDENCIES=
JOB_DONE=job_output/star/star_align.2.SRX377956.057ea75aa598f7ade9319a2e8ebaa132.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.SRX377956.057ea75aa598f7ade9319a2e8ebaa132.mugqic.done'
module load mugqic/star/2.5.3a && \
mkdir -p alignment/Blastocyst_rep1/SRX377956 && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/Blastocyst_rep1/SRX377956.trim.single.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/Blastocyst_rep1/SRX377956/ \
  --outSAMattrRGline ID:"SRX377956" 	PL:"ILLUMINA" 			SM:"Blastocyst_rep1" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 100000000000 \
  --limitBAMsortRAM 100000000000 \
  --limitIObufferSize 4000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f SRX377956/Aligned.sortedByCoord.out.bam alignment/Blastocyst_rep1/Blastocyst_rep1.sorted.bam
star_align.2.SRX377956.057ea75aa598f7ade9319a2e8ebaa132.mugqic.done
)
star_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=128G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$star_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: star_5_JOB_ID: star_align.2.SRX377957
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.SRX377957
JOB_DEPENDENCIES=
JOB_DONE=job_output/star/star_align.2.SRX377957.d898ad5e29d72891a30ac43ada2144bc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.SRX377957.d898ad5e29d72891a30ac43ada2144bc.mugqic.done'
module load mugqic/star/2.5.3a && \
mkdir -p alignment/Blastocyst_rep2/SRX377957 && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/Blastocyst_rep2/SRX377957.trim.single.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/Blastocyst_rep2/SRX377957/ \
  --outSAMattrRGline ID:"SRX377957" 	PL:"ILLUMINA" 			SM:"Blastocyst_rep2" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 100000000000 \
  --limitBAMsortRAM 100000000000 \
  --limitIObufferSize 4000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f SRX377957/Aligned.sortedByCoord.out.bam alignment/Blastocyst_rep2/Blastocyst_rep2.sorted.bam
star_align.2.SRX377957.d898ad5e29d72891a30ac43ada2144bc.mugqic.done
)
star_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=128G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$star_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: star_6_JOB_ID: star_align.2.SRX377958
#-------------------------------------------------------------------------------
JOB_NAME=star_align.2.SRX377958
JOB_DEPENDENCIES=
JOB_DONE=job_output/star/star_align.2.SRX377958.c38883e11546a81a614b69aa36a4b0ed.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_align.2.SRX377958.c38883e11546a81a614b69aa36a4b0ed.mugqic.done'
module load mugqic/star/2.5.3a && \
mkdir -p alignment/Blastocyst_rep3/SRX377958 && \
STAR --runMode alignReads \
  --genomeDir reference.Merged \
  --readFilesIn \
    trim/Blastocyst_rep3/SRX377958.trim.single.fastq.gz \
  --runThreadN 16 \
  --readFilesCommand zcat \
  --outStd Log \
  --outSAMunmapped Within \
  --outSAMtype BAM SortedByCoordinate \
  --outFileNamePrefix alignment/Blastocyst_rep3/SRX377958/ \
  --outSAMattrRGline ID:"SRX377958" 	PL:"ILLUMINA" 			SM:"Blastocyst_rep3" 	CN:"McGill University and Genome Quebec Innovation Centre"  \
  --limitGenomeGenerateRAM 100000000000 \
  --limitBAMsortRAM 100000000000 \
  --limitIObufferSize 4000000000 \
  --outWigType wiggle read1_5p --outWigStrand Stranded --outWigReferencesPrefix chr \
  --chimSegmentMin 21 && \
ln -s -f SRX377958/Aligned.sortedByCoord.out.bam alignment/Blastocyst_rep3/Blastocyst_rep3.sorted.bam
star_align.2.SRX377958.c38883e11546a81a614b69aa36a4b0ed.mugqic.done
)
star_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"star\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=128G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$star_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: star_7_JOB_ID: star_report
#-------------------------------------------------------------------------------
JOB_NAME=star_report
JOB_DEPENDENCIES=$star_1_JOB_ID:$star_2_JOB_ID:$star_3_JOB_ID:$star_4_JOB_ID:$star_5_JOB_ID:$star_6_JOB_ID
JOB_DONE=job_output/star/star_report.d1b92d13cb85e37cdd7045559a29a131.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'star_report.d1b92d13cb85e37cdd7045559a29a131.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
pandoc --to=markdown \
  --template /home/efournie/genpipes/bfx/report/RnaSeq.star.md \
  --variable scientific_name="Bos_taurus" \
  --variable assembly="UMD3.1" \
  /home/efournie/genpipes/bfx/report/RnaSeq.star.md \
  > report/RnaSeq.star.md
star_report.d1b92d13cb85e37cdd7045559a29a131.mugqic.done
)
star_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$star_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: picard_sort_sam
#-------------------------------------------------------------------------------
STEP=picard_sort_sam
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_1_JOB_ID: picard_sort_sam.GV-oocyte_rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.GV-oocyte_rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.GV-oocyte_rep1.9cca2da5a63be5371be642ea906d0db9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.GV-oocyte_rep1.9cca2da5a63be5371be642ea906d0db9.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/GV-oocyte_rep1/GV-oocyte_rep1.sorted.bam \
 OUTPUT=alignment/GV-oocyte_rep1/GV-oocyte_rep1.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.GV-oocyte_rep1.9cca2da5a63be5371be642ea906d0db9.mugqic.done
)
picard_sort_sam_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_2_JOB_ID: picard_sort_sam.GV-oocyte_rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.GV-oocyte_rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.GV-oocyte_rep2.4eeb5ab4141330c8391031687ccd45fa.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.GV-oocyte_rep2.4eeb5ab4141330c8391031687ccd45fa.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/GV-oocyte_rep2/GV-oocyte_rep2.sorted.bam \
 OUTPUT=alignment/GV-oocyte_rep2/GV-oocyte_rep2.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.GV-oocyte_rep2.4eeb5ab4141330c8391031687ccd45fa.mugqic.done
)
picard_sort_sam_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_3_JOB_ID: picard_sort_sam.GV-oocyte_rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.GV-oocyte_rep3
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.GV-oocyte_rep3.090a5f65db0b2d9fdd4c2c63e5600119.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.GV-oocyte_rep3.090a5f65db0b2d9fdd4c2c63e5600119.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/GV-oocyte_rep3/GV-oocyte_rep3.sorted.bam \
 OUTPUT=alignment/GV-oocyte_rep3/GV-oocyte_rep3.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.GV-oocyte_rep3.090a5f65db0b2d9fdd4c2c63e5600119.mugqic.done
)
picard_sort_sam_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_4_JOB_ID: picard_sort_sam.MII-oocyte_rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.MII-oocyte_rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.MII-oocyte_rep1.2679869141b6e73091e0adc8bd646a52.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.MII-oocyte_rep1.2679869141b6e73091e0adc8bd646a52.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/MII-oocyte_rep1/MII-oocyte_rep1.sorted.bam \
 OUTPUT=alignment/MII-oocyte_rep1/MII-oocyte_rep1.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.MII-oocyte_rep1.2679869141b6e73091e0adc8bd646a52.mugqic.done
)
picard_sort_sam_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_5_JOB_ID: picard_sort_sam.MII-oocyte_rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.MII-oocyte_rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.MII-oocyte_rep2.14e20b20076fd88265e6290cfc8917d1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.MII-oocyte_rep2.14e20b20076fd88265e6290cfc8917d1.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/MII-oocyte_rep2/MII-oocyte_rep2.sorted.bam \
 OUTPUT=alignment/MII-oocyte_rep2/MII-oocyte_rep2.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.MII-oocyte_rep2.14e20b20076fd88265e6290cfc8917d1.mugqic.done
)
picard_sort_sam_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_6_JOB_ID: picard_sort_sam.MII-oocyte_rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.MII-oocyte_rep3
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.MII-oocyte_rep3.7cb5e97a48b0732448fe1aa8eba0ac08.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.MII-oocyte_rep3.7cb5e97a48b0732448fe1aa8eba0ac08.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/MII-oocyte_rep3/MII-oocyte_rep3.sorted.bam \
 OUTPUT=alignment/MII-oocyte_rep3/MII-oocyte_rep3.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.MII-oocyte_rep3.7cb5e97a48b0732448fe1aa8eba0ac08.mugqic.done
)
picard_sort_sam_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_7_JOB_ID: picard_sort_sam.4-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.4-cell_rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.4-cell_rep1.2a96ac4b403d1723199fdb70e653ff7c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.4-cell_rep1.2a96ac4b403d1723199fdb70e653ff7c.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/4-cell_rep1/4-cell_rep1.sorted.bam \
 OUTPUT=alignment/4-cell_rep1/4-cell_rep1.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.4-cell_rep1.2a96ac4b403d1723199fdb70e653ff7c.mugqic.done
)
picard_sort_sam_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_8_JOB_ID: picard_sort_sam.4-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.4-cell_rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.4-cell_rep2.930889577212e3549ae2b5d61b3ea242.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.4-cell_rep2.930889577212e3549ae2b5d61b3ea242.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/4-cell_rep2/4-cell_rep2.sorted.bam \
 OUTPUT=alignment/4-cell_rep2/4-cell_rep2.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.4-cell_rep2.930889577212e3549ae2b5d61b3ea242.mugqic.done
)
picard_sort_sam_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_9_JOB_ID: picard_sort_sam.4-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.4-cell_rep3
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.4-cell_rep3.eaec6f5af90167ca4eba1572e02f17af.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.4-cell_rep3.eaec6f5af90167ca4eba1572e02f17af.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/4-cell_rep3/4-cell_rep3.sorted.bam \
 OUTPUT=alignment/4-cell_rep3/4-cell_rep3.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.4-cell_rep3.eaec6f5af90167ca4eba1572e02f17af.mugqic.done
)
picard_sort_sam_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_10_JOB_ID: picard_sort_sam.8-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.8-cell_rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.8-cell_rep1.217476aa52d00071353008876598e2c2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.8-cell_rep1.217476aa52d00071353008876598e2c2.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/8-cell_rep1/8-cell_rep1.sorted.bam \
 OUTPUT=alignment/8-cell_rep1/8-cell_rep1.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.8-cell_rep1.217476aa52d00071353008876598e2c2.mugqic.done
)
picard_sort_sam_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_11_JOB_ID: picard_sort_sam.8-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.8-cell_rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.8-cell_rep2.4b1cb801f2fc969d72bcb874f098c572.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.8-cell_rep2.4b1cb801f2fc969d72bcb874f098c572.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/8-cell_rep2/8-cell_rep2.sorted.bam \
 OUTPUT=alignment/8-cell_rep2/8-cell_rep2.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.8-cell_rep2.4b1cb801f2fc969d72bcb874f098c572.mugqic.done
)
picard_sort_sam_11_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_12_JOB_ID: picard_sort_sam.8-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.8-cell_rep3
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.8-cell_rep3.44799f535facbe89d0a197bb4e306095.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.8-cell_rep3.44799f535facbe89d0a197bb4e306095.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/8-cell_rep3/8-cell_rep3.sorted.bam \
 OUTPUT=alignment/8-cell_rep3/8-cell_rep3.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.8-cell_rep3.44799f535facbe89d0a197bb4e306095.mugqic.done
)
picard_sort_sam_12_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_13_JOB_ID: picard_sort_sam.16-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.16-cell_rep1
JOB_DEPENDENCIES=$star_1_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.16-cell_rep1.bdb74e71d36380eb3ed0c730234e71af.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.16-cell_rep1.bdb74e71d36380eb3ed0c730234e71af.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/16-cell_rep1/16-cell_rep1.sorted.bam \
 OUTPUT=alignment/16-cell_rep1/16-cell_rep1.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.16-cell_rep1.bdb74e71d36380eb3ed0c730234e71af.mugqic.done
)
picard_sort_sam_13_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_14_JOB_ID: picard_sort_sam.16-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.16-cell_rep2
JOB_DEPENDENCIES=$star_2_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.16-cell_rep2.8b1284dc47cfb67d6943dd5e10bd3ee5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.16-cell_rep2.8b1284dc47cfb67d6943dd5e10bd3ee5.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/16-cell_rep2/16-cell_rep2.sorted.bam \
 OUTPUT=alignment/16-cell_rep2/16-cell_rep2.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.16-cell_rep2.8b1284dc47cfb67d6943dd5e10bd3ee5.mugqic.done
)
picard_sort_sam_14_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_15_JOB_ID: picard_sort_sam.16-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.16-cell_rep3
JOB_DEPENDENCIES=$star_3_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.16-cell_rep3.4fc7dae4d89fe76901a1d58c89c420bd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.16-cell_rep3.4fc7dae4d89fe76901a1d58c89c420bd.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/16-cell_rep3/16-cell_rep3.sorted.bam \
 OUTPUT=alignment/16-cell_rep3/16-cell_rep3.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.16-cell_rep3.4fc7dae4d89fe76901a1d58c89c420bd.mugqic.done
)
picard_sort_sam_15_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_16_JOB_ID: picard_sort_sam.Blastocyst_rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.Blastocyst_rep1
JOB_DEPENDENCIES=$star_4_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.Blastocyst_rep1.40eb9eab9f69e9dc97425cb28ceb1bae.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.Blastocyst_rep1.40eb9eab9f69e9dc97425cb28ceb1bae.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Blastocyst_rep1/Blastocyst_rep1.sorted.bam \
 OUTPUT=alignment/Blastocyst_rep1/Blastocyst_rep1.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.Blastocyst_rep1.40eb9eab9f69e9dc97425cb28ceb1bae.mugqic.done
)
picard_sort_sam_16_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_17_JOB_ID: picard_sort_sam.Blastocyst_rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.Blastocyst_rep2
JOB_DEPENDENCIES=$star_5_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.Blastocyst_rep2.1a2c7718d008abb1d9423d41af369858.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.Blastocyst_rep2.1a2c7718d008abb1d9423d41af369858.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Blastocyst_rep2/Blastocyst_rep2.sorted.bam \
 OUTPUT=alignment/Blastocyst_rep2/Blastocyst_rep2.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.Blastocyst_rep2.1a2c7718d008abb1d9423d41af369858.mugqic.done
)
picard_sort_sam_17_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_sort_sam_18_JOB_ID: picard_sort_sam.Blastocyst_rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_sort_sam.Blastocyst_rep3
JOB_DEPENDENCIES=$star_6_JOB_ID
JOB_DONE=job_output/picard_sort_sam/picard_sort_sam.Blastocyst_rep3.95cc38e0d1b82ef89e457103422db02a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_sort_sam.Blastocyst_rep3.95cc38e0d1b82ef89e457103422db02a.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Blastocyst_rep3/Blastocyst_rep3.sorted.bam \
 OUTPUT=alignment/Blastocyst_rep3/Blastocyst_rep3.QueryNameSorted.bam \
 SORT_ORDER=queryname \
 MAX_RECORDS_IN_RAM=5750000
picard_sort_sam.Blastocyst_rep3.95cc38e0d1b82ef89e457103422db02a.mugqic.done
)
picard_sort_sam_18_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_sort_sam\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_sort_sam_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: picard_mark_duplicates
#-------------------------------------------------------------------------------
STEP=picard_mark_duplicates
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_1_JOB_ID: picard_mark_duplicates.GV-oocyte_rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.GV-oocyte_rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.GV-oocyte_rep1.0a76c0442953bee6787fecb1ee35602c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.GV-oocyte_rep1.0a76c0442953bee6787fecb1ee35602c.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/GV-oocyte_rep1/GV-oocyte_rep1.sorted.bam \
 OUTPUT=alignment/GV-oocyte_rep1/GV-oocyte_rep1.sorted.mdup.bam \
 METRICS_FILE=alignment/GV-oocyte_rep1/GV-oocyte_rep1.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.GV-oocyte_rep1.0a76c0442953bee6787fecb1ee35602c.mugqic.done
)
picard_mark_duplicates_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_2_JOB_ID: picard_mark_duplicates.GV-oocyte_rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.GV-oocyte_rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.GV-oocyte_rep2.8d9072f9de68066e767d6012772fb3bf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.GV-oocyte_rep2.8d9072f9de68066e767d6012772fb3bf.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/GV-oocyte_rep2/GV-oocyte_rep2.sorted.bam \
 OUTPUT=alignment/GV-oocyte_rep2/GV-oocyte_rep2.sorted.mdup.bam \
 METRICS_FILE=alignment/GV-oocyte_rep2/GV-oocyte_rep2.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.GV-oocyte_rep2.8d9072f9de68066e767d6012772fb3bf.mugqic.done
)
picard_mark_duplicates_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_3_JOB_ID: picard_mark_duplicates.GV-oocyte_rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.GV-oocyte_rep3
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.GV-oocyte_rep3.6f20acc1012dd84cbf3f0edd00fc9e8e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.GV-oocyte_rep3.6f20acc1012dd84cbf3f0edd00fc9e8e.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/GV-oocyte_rep3/GV-oocyte_rep3.sorted.bam \
 OUTPUT=alignment/GV-oocyte_rep3/GV-oocyte_rep3.sorted.mdup.bam \
 METRICS_FILE=alignment/GV-oocyte_rep3/GV-oocyte_rep3.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.GV-oocyte_rep3.6f20acc1012dd84cbf3f0edd00fc9e8e.mugqic.done
)
picard_mark_duplicates_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_4_JOB_ID: picard_mark_duplicates.MII-oocyte_rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.MII-oocyte_rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.MII-oocyte_rep1.13548da04a660e1ebedcc6cddff700ad.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.MII-oocyte_rep1.13548da04a660e1ebedcc6cddff700ad.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/MII-oocyte_rep1/MII-oocyte_rep1.sorted.bam \
 OUTPUT=alignment/MII-oocyte_rep1/MII-oocyte_rep1.sorted.mdup.bam \
 METRICS_FILE=alignment/MII-oocyte_rep1/MII-oocyte_rep1.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.MII-oocyte_rep1.13548da04a660e1ebedcc6cddff700ad.mugqic.done
)
picard_mark_duplicates_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_5_JOB_ID: picard_mark_duplicates.MII-oocyte_rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.MII-oocyte_rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.MII-oocyte_rep2.d858503be47f930caa40e12df89b236f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.MII-oocyte_rep2.d858503be47f930caa40e12df89b236f.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/MII-oocyte_rep2/MII-oocyte_rep2.sorted.bam \
 OUTPUT=alignment/MII-oocyte_rep2/MII-oocyte_rep2.sorted.mdup.bam \
 METRICS_FILE=alignment/MII-oocyte_rep2/MII-oocyte_rep2.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.MII-oocyte_rep2.d858503be47f930caa40e12df89b236f.mugqic.done
)
picard_mark_duplicates_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_6_JOB_ID: picard_mark_duplicates.MII-oocyte_rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.MII-oocyte_rep3
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.MII-oocyte_rep3.7311b5edc3ba9c9b92aae1b6e39773d6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.MII-oocyte_rep3.7311b5edc3ba9c9b92aae1b6e39773d6.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/MII-oocyte_rep3/MII-oocyte_rep3.sorted.bam \
 OUTPUT=alignment/MII-oocyte_rep3/MII-oocyte_rep3.sorted.mdup.bam \
 METRICS_FILE=alignment/MII-oocyte_rep3/MII-oocyte_rep3.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.MII-oocyte_rep3.7311b5edc3ba9c9b92aae1b6e39773d6.mugqic.done
)
picard_mark_duplicates_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_7_JOB_ID: picard_mark_duplicates.4-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.4-cell_rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.4-cell_rep1.361dfbc39aed759098a119a63a9fbbe7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.4-cell_rep1.361dfbc39aed759098a119a63a9fbbe7.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/4-cell_rep1/4-cell_rep1.sorted.bam \
 OUTPUT=alignment/4-cell_rep1/4-cell_rep1.sorted.mdup.bam \
 METRICS_FILE=alignment/4-cell_rep1/4-cell_rep1.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.4-cell_rep1.361dfbc39aed759098a119a63a9fbbe7.mugqic.done
)
picard_mark_duplicates_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_8_JOB_ID: picard_mark_duplicates.4-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.4-cell_rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.4-cell_rep2.9982f4cd97518bea8dac82904b82bb60.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.4-cell_rep2.9982f4cd97518bea8dac82904b82bb60.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/4-cell_rep2/4-cell_rep2.sorted.bam \
 OUTPUT=alignment/4-cell_rep2/4-cell_rep2.sorted.mdup.bam \
 METRICS_FILE=alignment/4-cell_rep2/4-cell_rep2.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.4-cell_rep2.9982f4cd97518bea8dac82904b82bb60.mugqic.done
)
picard_mark_duplicates_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_9_JOB_ID: picard_mark_duplicates.4-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.4-cell_rep3
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.4-cell_rep3.2116818d0c1ed09ed1b79b54017e299b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.4-cell_rep3.2116818d0c1ed09ed1b79b54017e299b.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/4-cell_rep3/4-cell_rep3.sorted.bam \
 OUTPUT=alignment/4-cell_rep3/4-cell_rep3.sorted.mdup.bam \
 METRICS_FILE=alignment/4-cell_rep3/4-cell_rep3.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.4-cell_rep3.2116818d0c1ed09ed1b79b54017e299b.mugqic.done
)
picard_mark_duplicates_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_10_JOB_ID: picard_mark_duplicates.8-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.8-cell_rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.8-cell_rep1.ae802fac5c11cfffc7f79c53678d93bb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.8-cell_rep1.ae802fac5c11cfffc7f79c53678d93bb.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/8-cell_rep1/8-cell_rep1.sorted.bam \
 OUTPUT=alignment/8-cell_rep1/8-cell_rep1.sorted.mdup.bam \
 METRICS_FILE=alignment/8-cell_rep1/8-cell_rep1.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.8-cell_rep1.ae802fac5c11cfffc7f79c53678d93bb.mugqic.done
)
picard_mark_duplicates_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_11_JOB_ID: picard_mark_duplicates.8-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.8-cell_rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.8-cell_rep2.ad5464c7ce49b9ef0d39030d51cd03b8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.8-cell_rep2.ad5464c7ce49b9ef0d39030d51cd03b8.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/8-cell_rep2/8-cell_rep2.sorted.bam \
 OUTPUT=alignment/8-cell_rep2/8-cell_rep2.sorted.mdup.bam \
 METRICS_FILE=alignment/8-cell_rep2/8-cell_rep2.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.8-cell_rep2.ad5464c7ce49b9ef0d39030d51cd03b8.mugqic.done
)
picard_mark_duplicates_11_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_12_JOB_ID: picard_mark_duplicates.8-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.8-cell_rep3
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.8-cell_rep3.110d287e30e16f92835859077e595a8a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.8-cell_rep3.110d287e30e16f92835859077e595a8a.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/8-cell_rep3/8-cell_rep3.sorted.bam \
 OUTPUT=alignment/8-cell_rep3/8-cell_rep3.sorted.mdup.bam \
 METRICS_FILE=alignment/8-cell_rep3/8-cell_rep3.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.8-cell_rep3.110d287e30e16f92835859077e595a8a.mugqic.done
)
picard_mark_duplicates_12_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_13_JOB_ID: picard_mark_duplicates.16-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.16-cell_rep1
JOB_DEPENDENCIES=$star_1_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.16-cell_rep1.35d2c1b4159b0c3938d1ab39b00ecf5b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.16-cell_rep1.35d2c1b4159b0c3938d1ab39b00ecf5b.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/16-cell_rep1/16-cell_rep1.sorted.bam \
 OUTPUT=alignment/16-cell_rep1/16-cell_rep1.sorted.mdup.bam \
 METRICS_FILE=alignment/16-cell_rep1/16-cell_rep1.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.16-cell_rep1.35d2c1b4159b0c3938d1ab39b00ecf5b.mugqic.done
)
picard_mark_duplicates_13_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_14_JOB_ID: picard_mark_duplicates.16-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.16-cell_rep2
JOB_DEPENDENCIES=$star_2_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.16-cell_rep2.5d3f1a9cb7a31aa81ebbc4c9f97c8cb0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.16-cell_rep2.5d3f1a9cb7a31aa81ebbc4c9f97c8cb0.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/16-cell_rep2/16-cell_rep2.sorted.bam \
 OUTPUT=alignment/16-cell_rep2/16-cell_rep2.sorted.mdup.bam \
 METRICS_FILE=alignment/16-cell_rep2/16-cell_rep2.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.16-cell_rep2.5d3f1a9cb7a31aa81ebbc4c9f97c8cb0.mugqic.done
)
picard_mark_duplicates_14_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_15_JOB_ID: picard_mark_duplicates.16-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.16-cell_rep3
JOB_DEPENDENCIES=$star_3_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.16-cell_rep3.3b20479a6b9986375adac8fe86d4c0e0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.16-cell_rep3.3b20479a6b9986375adac8fe86d4c0e0.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/16-cell_rep3/16-cell_rep3.sorted.bam \
 OUTPUT=alignment/16-cell_rep3/16-cell_rep3.sorted.mdup.bam \
 METRICS_FILE=alignment/16-cell_rep3/16-cell_rep3.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.16-cell_rep3.3b20479a6b9986375adac8fe86d4c0e0.mugqic.done
)
picard_mark_duplicates_15_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_16_JOB_ID: picard_mark_duplicates.Blastocyst_rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.Blastocyst_rep1
JOB_DEPENDENCIES=$star_4_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.Blastocyst_rep1.2d967d8cb67c155944052d52e2baf9a6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.Blastocyst_rep1.2d967d8cb67c155944052d52e2baf9a6.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Blastocyst_rep1/Blastocyst_rep1.sorted.bam \
 OUTPUT=alignment/Blastocyst_rep1/Blastocyst_rep1.sorted.mdup.bam \
 METRICS_FILE=alignment/Blastocyst_rep1/Blastocyst_rep1.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.Blastocyst_rep1.2d967d8cb67c155944052d52e2baf9a6.mugqic.done
)
picard_mark_duplicates_16_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_17_JOB_ID: picard_mark_duplicates.Blastocyst_rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.Blastocyst_rep2
JOB_DEPENDENCIES=$star_5_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.Blastocyst_rep2.50d347e2958174b7dc67301309b8f768.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.Blastocyst_rep2.50d347e2958174b7dc67301309b8f768.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Blastocyst_rep2/Blastocyst_rep2.sorted.bam \
 OUTPUT=alignment/Blastocyst_rep2/Blastocyst_rep2.sorted.mdup.bam \
 METRICS_FILE=alignment/Blastocyst_rep2/Blastocyst_rep2.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.Blastocyst_rep2.50d347e2958174b7dc67301309b8f768.mugqic.done
)
picard_mark_duplicates_17_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_18_JOB_ID: picard_mark_duplicates.Blastocyst_rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.Blastocyst_rep3
JOB_DEPENDENCIES=$star_6_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.Blastocyst_rep3.975defefd83583cc752250f92e98d593.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.Blastocyst_rep3.975defefd83583cc752250f92e98d593.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx14G -jar $PICARD_HOME/picard.jar MarkDuplicates \
 REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Blastocyst_rep3/Blastocyst_rep3.sorted.bam \
 OUTPUT=alignment/Blastocyst_rep3/Blastocyst_rep3.sorted.mdup.bam \
 METRICS_FILE=alignment/Blastocyst_rep3/Blastocyst_rep3.sorted.mdup.metrics \
 MAX_RECORDS_IN_RAM=3500000
picard_mark_duplicates.Blastocyst_rep3.975defefd83583cc752250f92e98d593.mugqic.done
)
picard_mark_duplicates_18_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_mark_duplicates\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=20G -N 1 -n 5 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_mark_duplicates_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: picard_rna_metrics
#-------------------------------------------------------------------------------
STEP=picard_rna_metrics
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_1_JOB_ID: picard_rna_metrics.GV-oocyte_rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.GV-oocyte_rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.GV-oocyte_rep1.fb29cdc668e3f540d1a8736399dc8a1f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.GV-oocyte_rep1.fb29cdc668e3f540d1a8736399dc8a1f.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/GV-oocyte_rep1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 INPUT=alignment/GV-oocyte_rep1/GV-oocyte_rep1.sorted.mdup.bam \
 OUTPUT=metrics/GV-oocyte_rep1/GV-oocyte_rep1 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/GV-oocyte_rep1/GV-oocyte_rep1.sorted.mdup.bam \
 OUTPUT=metrics/GV-oocyte_rep1/GV-oocyte_rep1.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.GV-oocyte_rep1.fb29cdc668e3f540d1a8736399dc8a1f.mugqic.done
)
picard_rna_metrics_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_2_JOB_ID: picard_rna_metrics.GV-oocyte_rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.GV-oocyte_rep2
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.GV-oocyte_rep2.284a897e52f78efdb28a1ea13b3aef0d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.GV-oocyte_rep2.284a897e52f78efdb28a1ea13b3aef0d.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/GV-oocyte_rep2 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 INPUT=alignment/GV-oocyte_rep2/GV-oocyte_rep2.sorted.mdup.bam \
 OUTPUT=metrics/GV-oocyte_rep2/GV-oocyte_rep2 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/GV-oocyte_rep2/GV-oocyte_rep2.sorted.mdup.bam \
 OUTPUT=metrics/GV-oocyte_rep2/GV-oocyte_rep2.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.GV-oocyte_rep2.284a897e52f78efdb28a1ea13b3aef0d.mugqic.done
)
picard_rna_metrics_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_3_JOB_ID: picard_rna_metrics.GV-oocyte_rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.GV-oocyte_rep3
JOB_DEPENDENCIES=$picard_mark_duplicates_3_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.GV-oocyte_rep3.58af105a84d49bacc808c56c3d6261d8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.GV-oocyte_rep3.58af105a84d49bacc808c56c3d6261d8.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/GV-oocyte_rep3 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 INPUT=alignment/GV-oocyte_rep3/GV-oocyte_rep3.sorted.mdup.bam \
 OUTPUT=metrics/GV-oocyte_rep3/GV-oocyte_rep3 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/GV-oocyte_rep3/GV-oocyte_rep3.sorted.mdup.bam \
 OUTPUT=metrics/GV-oocyte_rep3/GV-oocyte_rep3.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.GV-oocyte_rep3.58af105a84d49bacc808c56c3d6261d8.mugqic.done
)
picard_rna_metrics_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_4_JOB_ID: picard_rna_metrics.MII-oocyte_rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.MII-oocyte_rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.MII-oocyte_rep1.e555e331d0dd7481d910c55cd1a48c01.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.MII-oocyte_rep1.e555e331d0dd7481d910c55cd1a48c01.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/MII-oocyte_rep1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 INPUT=alignment/MII-oocyte_rep1/MII-oocyte_rep1.sorted.mdup.bam \
 OUTPUT=metrics/MII-oocyte_rep1/MII-oocyte_rep1 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/MII-oocyte_rep1/MII-oocyte_rep1.sorted.mdup.bam \
 OUTPUT=metrics/MII-oocyte_rep1/MII-oocyte_rep1.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.MII-oocyte_rep1.e555e331d0dd7481d910c55cd1a48c01.mugqic.done
)
picard_rna_metrics_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_5_JOB_ID: picard_rna_metrics.MII-oocyte_rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.MII-oocyte_rep2
JOB_DEPENDENCIES=$picard_mark_duplicates_5_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.MII-oocyte_rep2.d44133e941a5865d24f7cc3acd5005db.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.MII-oocyte_rep2.d44133e941a5865d24f7cc3acd5005db.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/MII-oocyte_rep2 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 INPUT=alignment/MII-oocyte_rep2/MII-oocyte_rep2.sorted.mdup.bam \
 OUTPUT=metrics/MII-oocyte_rep2/MII-oocyte_rep2 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/MII-oocyte_rep2/MII-oocyte_rep2.sorted.mdup.bam \
 OUTPUT=metrics/MII-oocyte_rep2/MII-oocyte_rep2.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.MII-oocyte_rep2.d44133e941a5865d24f7cc3acd5005db.mugqic.done
)
picard_rna_metrics_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_6_JOB_ID: picard_rna_metrics.MII-oocyte_rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.MII-oocyte_rep3
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.MII-oocyte_rep3.18903e6f097e50c28dade36a477ff56e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.MII-oocyte_rep3.18903e6f097e50c28dade36a477ff56e.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/MII-oocyte_rep3 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 INPUT=alignment/MII-oocyte_rep3/MII-oocyte_rep3.sorted.mdup.bam \
 OUTPUT=metrics/MII-oocyte_rep3/MII-oocyte_rep3 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/MII-oocyte_rep3/MII-oocyte_rep3.sorted.mdup.bam \
 OUTPUT=metrics/MII-oocyte_rep3/MII-oocyte_rep3.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.MII-oocyte_rep3.18903e6f097e50c28dade36a477ff56e.mugqic.done
)
picard_rna_metrics_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_7_JOB_ID: picard_rna_metrics.4-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.4-cell_rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_7_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.4-cell_rep1.989ba4d9e7ca4bcf31a04b8a6e3b92df.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.4-cell_rep1.989ba4d9e7ca4bcf31a04b8a6e3b92df.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/4-cell_rep1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 INPUT=alignment/4-cell_rep1/4-cell_rep1.sorted.mdup.bam \
 OUTPUT=metrics/4-cell_rep1/4-cell_rep1 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/4-cell_rep1/4-cell_rep1.sorted.mdup.bam \
 OUTPUT=metrics/4-cell_rep1/4-cell_rep1.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.4-cell_rep1.989ba4d9e7ca4bcf31a04b8a6e3b92df.mugqic.done
)
picard_rna_metrics_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_8_JOB_ID: picard_rna_metrics.4-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.4-cell_rep2
JOB_DEPENDENCIES=$picard_mark_duplicates_8_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.4-cell_rep2.72e1b783367241a81a7e26b4977ba259.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.4-cell_rep2.72e1b783367241a81a7e26b4977ba259.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/4-cell_rep2 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 INPUT=alignment/4-cell_rep2/4-cell_rep2.sorted.mdup.bam \
 OUTPUT=metrics/4-cell_rep2/4-cell_rep2 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/4-cell_rep2/4-cell_rep2.sorted.mdup.bam \
 OUTPUT=metrics/4-cell_rep2/4-cell_rep2.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.4-cell_rep2.72e1b783367241a81a7e26b4977ba259.mugqic.done
)
picard_rna_metrics_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_9_JOB_ID: picard_rna_metrics.4-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.4-cell_rep3
JOB_DEPENDENCIES=$picard_mark_duplicates_9_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.4-cell_rep3.4b96e064144ad255d7fa73b958deb514.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.4-cell_rep3.4b96e064144ad255d7fa73b958deb514.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/4-cell_rep3 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 INPUT=alignment/4-cell_rep3/4-cell_rep3.sorted.mdup.bam \
 OUTPUT=metrics/4-cell_rep3/4-cell_rep3 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/4-cell_rep3/4-cell_rep3.sorted.mdup.bam \
 OUTPUT=metrics/4-cell_rep3/4-cell_rep3.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.4-cell_rep3.4b96e064144ad255d7fa73b958deb514.mugqic.done
)
picard_rna_metrics_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_10_JOB_ID: picard_rna_metrics.8-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.8-cell_rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_10_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.8-cell_rep1.fc32f74016509082ae9d708e68413bdd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.8-cell_rep1.fc32f74016509082ae9d708e68413bdd.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/8-cell_rep1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 INPUT=alignment/8-cell_rep1/8-cell_rep1.sorted.mdup.bam \
 OUTPUT=metrics/8-cell_rep1/8-cell_rep1 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/8-cell_rep1/8-cell_rep1.sorted.mdup.bam \
 OUTPUT=metrics/8-cell_rep1/8-cell_rep1.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.8-cell_rep1.fc32f74016509082ae9d708e68413bdd.mugqic.done
)
picard_rna_metrics_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_11_JOB_ID: picard_rna_metrics.8-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.8-cell_rep2
JOB_DEPENDENCIES=$picard_mark_duplicates_11_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.8-cell_rep2.5e94a3464694af1433e0b9863d5c429d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.8-cell_rep2.5e94a3464694af1433e0b9863d5c429d.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/8-cell_rep2 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 INPUT=alignment/8-cell_rep2/8-cell_rep2.sorted.mdup.bam \
 OUTPUT=metrics/8-cell_rep2/8-cell_rep2 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/8-cell_rep2/8-cell_rep2.sorted.mdup.bam \
 OUTPUT=metrics/8-cell_rep2/8-cell_rep2.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.8-cell_rep2.5e94a3464694af1433e0b9863d5c429d.mugqic.done
)
picard_rna_metrics_11_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_12_JOB_ID: picard_rna_metrics.8-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.8-cell_rep3
JOB_DEPENDENCIES=$picard_mark_duplicates_12_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.8-cell_rep3.d817ad88601822d601c410dab997dc66.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.8-cell_rep3.d817ad88601822d601c410dab997dc66.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/8-cell_rep3 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 INPUT=alignment/8-cell_rep3/8-cell_rep3.sorted.mdup.bam \
 OUTPUT=metrics/8-cell_rep3/8-cell_rep3 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/8-cell_rep3/8-cell_rep3.sorted.mdup.bam \
 OUTPUT=metrics/8-cell_rep3/8-cell_rep3.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.8-cell_rep3.d817ad88601822d601c410dab997dc66.mugqic.done
)
picard_rna_metrics_12_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_13_JOB_ID: picard_rna_metrics.16-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.16-cell_rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_13_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.16-cell_rep1.363407186796c573fee61361b360d8b8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.16-cell_rep1.363407186796c573fee61361b360d8b8.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/16-cell_rep1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 INPUT=alignment/16-cell_rep1/16-cell_rep1.sorted.mdup.bam \
 OUTPUT=metrics/16-cell_rep1/16-cell_rep1 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/16-cell_rep1/16-cell_rep1.sorted.mdup.bam \
 OUTPUT=metrics/16-cell_rep1/16-cell_rep1.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.16-cell_rep1.363407186796c573fee61361b360d8b8.mugqic.done
)
picard_rna_metrics_13_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_14_JOB_ID: picard_rna_metrics.16-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.16-cell_rep2
JOB_DEPENDENCIES=$picard_mark_duplicates_14_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.16-cell_rep2.64ecd8db819eac9af9a457e0649c6dbf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.16-cell_rep2.64ecd8db819eac9af9a457e0649c6dbf.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/16-cell_rep2 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 INPUT=alignment/16-cell_rep2/16-cell_rep2.sorted.mdup.bam \
 OUTPUT=metrics/16-cell_rep2/16-cell_rep2 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/16-cell_rep2/16-cell_rep2.sorted.mdup.bam \
 OUTPUT=metrics/16-cell_rep2/16-cell_rep2.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.16-cell_rep2.64ecd8db819eac9af9a457e0649c6dbf.mugqic.done
)
picard_rna_metrics_14_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_15_JOB_ID: picard_rna_metrics.16-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.16-cell_rep3
JOB_DEPENDENCIES=$picard_mark_duplicates_15_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.16-cell_rep3.9442db0e0ee63969bb1f38835c6ac87a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.16-cell_rep3.9442db0e0ee63969bb1f38835c6ac87a.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/16-cell_rep3 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 INPUT=alignment/16-cell_rep3/16-cell_rep3.sorted.mdup.bam \
 OUTPUT=metrics/16-cell_rep3/16-cell_rep3 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/16-cell_rep3/16-cell_rep3.sorted.mdup.bam \
 OUTPUT=metrics/16-cell_rep3/16-cell_rep3.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.16-cell_rep3.9442db0e0ee63969bb1f38835c6ac87a.mugqic.done
)
picard_rna_metrics_15_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_16_JOB_ID: picard_rna_metrics.Blastocyst_rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Blastocyst_rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_16_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Blastocyst_rep1.c45233ef18ade00ee7538e7f6d802f26.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Blastocyst_rep1.c45233ef18ade00ee7538e7f6d802f26.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Blastocyst_rep1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 INPUT=alignment/Blastocyst_rep1/Blastocyst_rep1.sorted.mdup.bam \
 OUTPUT=metrics/Blastocyst_rep1/Blastocyst_rep1 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Blastocyst_rep1/Blastocyst_rep1.sorted.mdup.bam \
 OUTPUT=metrics/Blastocyst_rep1/Blastocyst_rep1.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Blastocyst_rep1.c45233ef18ade00ee7538e7f6d802f26.mugqic.done
)
picard_rna_metrics_16_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_17_JOB_ID: picard_rna_metrics.Blastocyst_rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Blastocyst_rep2
JOB_DEPENDENCIES=$picard_mark_duplicates_17_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Blastocyst_rep2.c6f99d548eae0517709896fbdc33e3b3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Blastocyst_rep2.c6f99d548eae0517709896fbdc33e3b3.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Blastocyst_rep2 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 INPUT=alignment/Blastocyst_rep2/Blastocyst_rep2.sorted.mdup.bam \
 OUTPUT=metrics/Blastocyst_rep2/Blastocyst_rep2 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Blastocyst_rep2/Blastocyst_rep2.sorted.mdup.bam \
 OUTPUT=metrics/Blastocyst_rep2/Blastocyst_rep2.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Blastocyst_rep2.c6f99d548eae0517709896fbdc33e3b3.mugqic.done
)
picard_rna_metrics_17_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_18_JOB_ID: picard_rna_metrics.Blastocyst_rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Blastocyst_rep3
JOB_DEPENDENCIES=$picard_mark_duplicates_18_JOB_ID
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Blastocyst_rep3.4c6fa8aed360a177cf7bd8931255b20f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Blastocyst_rep3.4c6fa8aed360a177cf7bd8931255b20f.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Blastocyst_rep3 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 INPUT=alignment/Blastocyst_rep3/Blastocyst_rep3.sorted.mdup.bam \
 OUTPUT=metrics/Blastocyst_rep3/Blastocyst_rep3 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Blastocyst_rep3/Blastocyst_rep3.sorted.mdup.bam \
 OUTPUT=metrics/Blastocyst_rep3/Blastocyst_rep3.picard_rna_metrics \
 REF_FLAT=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=$MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Blastocyst_rep3.4c6fa8aed360a177cf7bd8931255b20f.mugqic.done
)
picard_rna_metrics_18_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: estimate_ribosomal_rna
#-------------------------------------------------------------------------------
STEP=estimate_ribosomal_rna
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_1_JOB_ID: bwa_mem_rRNA.SRX377941
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.SRX377941
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.SRX377941.f947ef5fa7d6f8425a7bfa14452579dc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.SRX377941.f947ef5fa7d6f8425a7bfa14452579dc.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/GV-oocyte_rep1/SRX377941 metrics/GV-oocyte_rep1/SRX377941 && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/GV-oocyte_rep1/SRX377941/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:SRX377941	SM:GV-oocyte_rep1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/rrna_bwa_index/Bos_taurus.UMD3.1.Ensembl84.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/GV-oocyte_rep1/SRX377941/SRX377941rRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/GV-oocyte_rep1/SRX377941/SRX377941rRNA.bam \
  -g $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  -o metrics/GV-oocyte_rep1/SRX377941/SRX377941rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.SRX377941.f947ef5fa7d6f8425a7bfa14452579dc.mugqic.done
)
estimate_ribosomal_rna_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_2_JOB_ID: bwa_mem_rRNA.SRX377942
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.SRX377942
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.SRX377942.a70c33e7c7971758f361421d96c41fee.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.SRX377942.a70c33e7c7971758f361421d96c41fee.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/GV-oocyte_rep2/SRX377942 metrics/GV-oocyte_rep2/SRX377942 && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/GV-oocyte_rep2/SRX377942/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:SRX377942	SM:GV-oocyte_rep2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/rrna_bwa_index/Bos_taurus.UMD3.1.Ensembl84.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/GV-oocyte_rep2/SRX377942/SRX377942rRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/GV-oocyte_rep2/SRX377942/SRX377942rRNA.bam \
  -g $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  -o metrics/GV-oocyte_rep2/SRX377942/SRX377942rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.SRX377942.a70c33e7c7971758f361421d96c41fee.mugqic.done
)
estimate_ribosomal_rna_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_3_JOB_ID: bwa_mem_rRNA.SRX377943
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.SRX377943
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.SRX377943.1df524fe4343ba86e8979e98af588d46.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.SRX377943.1df524fe4343ba86e8979e98af588d46.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/GV-oocyte_rep3/SRX377943 metrics/GV-oocyte_rep3/SRX377943 && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/GV-oocyte_rep3/SRX377943/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:SRX377943	SM:GV-oocyte_rep3	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/rrna_bwa_index/Bos_taurus.UMD3.1.Ensembl84.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/GV-oocyte_rep3/SRX377943/SRX377943rRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/GV-oocyte_rep3/SRX377943/SRX377943rRNA.bam \
  -g $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  -o metrics/GV-oocyte_rep3/SRX377943/SRX377943rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.SRX377943.1df524fe4343ba86e8979e98af588d46.mugqic.done
)
estimate_ribosomal_rna_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_4_JOB_ID: bwa_mem_rRNA.SRX377944
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.SRX377944
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.SRX377944.ec8f9209c60b0ec520b2e647c5f45bf0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.SRX377944.ec8f9209c60b0ec520b2e647c5f45bf0.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/MII-oocyte_rep1/SRX377944 metrics/MII-oocyte_rep1/SRX377944 && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/MII-oocyte_rep1/SRX377944/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:SRX377944	SM:MII-oocyte_rep1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/rrna_bwa_index/Bos_taurus.UMD3.1.Ensembl84.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/MII-oocyte_rep1/SRX377944/SRX377944rRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/MII-oocyte_rep1/SRX377944/SRX377944rRNA.bam \
  -g $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  -o metrics/MII-oocyte_rep1/SRX377944/SRX377944rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.SRX377944.ec8f9209c60b0ec520b2e647c5f45bf0.mugqic.done
)
estimate_ribosomal_rna_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_5_JOB_ID: bwa_mem_rRNA.SRX377945
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.SRX377945
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.SRX377945.fd6b5c9f6f760f53aa2f9e2ab69c4068.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.SRX377945.fd6b5c9f6f760f53aa2f9e2ab69c4068.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/MII-oocyte_rep2/SRX377945 metrics/MII-oocyte_rep2/SRX377945 && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/MII-oocyte_rep2/SRX377945/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:SRX377945	SM:MII-oocyte_rep2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/rrna_bwa_index/Bos_taurus.UMD3.1.Ensembl84.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/MII-oocyte_rep2/SRX377945/SRX377945rRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/MII-oocyte_rep2/SRX377945/SRX377945rRNA.bam \
  -g $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  -o metrics/MII-oocyte_rep2/SRX377945/SRX377945rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.SRX377945.fd6b5c9f6f760f53aa2f9e2ab69c4068.mugqic.done
)
estimate_ribosomal_rna_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_6_JOB_ID: bwa_mem_rRNA.SRX377946
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.SRX377946
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.SRX377946.571654160457b0518886bf0f24ce035d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.SRX377946.571654160457b0518886bf0f24ce035d.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/MII-oocyte_rep3/SRX377946 metrics/MII-oocyte_rep3/SRX377946 && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/MII-oocyte_rep3/SRX377946/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:SRX377946	SM:MII-oocyte_rep3	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/rrna_bwa_index/Bos_taurus.UMD3.1.Ensembl84.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/MII-oocyte_rep3/SRX377946/SRX377946rRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/MII-oocyte_rep3/SRX377946/SRX377946rRNA.bam \
  -g $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  -o metrics/MII-oocyte_rep3/SRX377946/SRX377946rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.SRX377946.571654160457b0518886bf0f24ce035d.mugqic.done
)
estimate_ribosomal_rna_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_7_JOB_ID: bwa_mem_rRNA.SRX377947
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.SRX377947
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.SRX377947.25cbb3c01a66d1748bdc9a45f7d24768.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.SRX377947.25cbb3c01a66d1748bdc9a45f7d24768.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/4-cell_rep1/SRX377947 metrics/4-cell_rep1/SRX377947 && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/4-cell_rep1/SRX377947/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:SRX377947	SM:4-cell_rep1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/rrna_bwa_index/Bos_taurus.UMD3.1.Ensembl84.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/4-cell_rep1/SRX377947/SRX377947rRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/4-cell_rep1/SRX377947/SRX377947rRNA.bam \
  -g $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  -o metrics/4-cell_rep1/SRX377947/SRX377947rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.SRX377947.25cbb3c01a66d1748bdc9a45f7d24768.mugqic.done
)
estimate_ribosomal_rna_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_8_JOB_ID: bwa_mem_rRNA.SRX377948
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.SRX377948
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.SRX377948.7e6d9b4e45f8bf295cfe0af4efd9cc27.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.SRX377948.7e6d9b4e45f8bf295cfe0af4efd9cc27.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/4-cell_rep2/SRX377948 metrics/4-cell_rep2/SRX377948 && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/4-cell_rep2/SRX377948/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:SRX377948	SM:4-cell_rep2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/rrna_bwa_index/Bos_taurus.UMD3.1.Ensembl84.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/4-cell_rep2/SRX377948/SRX377948rRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/4-cell_rep2/SRX377948/SRX377948rRNA.bam \
  -g $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  -o metrics/4-cell_rep2/SRX377948/SRX377948rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.SRX377948.7e6d9b4e45f8bf295cfe0af4efd9cc27.mugqic.done
)
estimate_ribosomal_rna_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_9_JOB_ID: bwa_mem_rRNA.SRX377949
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.SRX377949
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.SRX377949.35bbe0f7cb3d0b0072cc35fbe68479f4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.SRX377949.35bbe0f7cb3d0b0072cc35fbe68479f4.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/4-cell_rep3/SRX377949 metrics/4-cell_rep3/SRX377949 && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/4-cell_rep3/SRX377949/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:SRX377949	SM:4-cell_rep3	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/rrna_bwa_index/Bos_taurus.UMD3.1.Ensembl84.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/4-cell_rep3/SRX377949/SRX377949rRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/4-cell_rep3/SRX377949/SRX377949rRNA.bam \
  -g $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  -o metrics/4-cell_rep3/SRX377949/SRX377949rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.SRX377949.35bbe0f7cb3d0b0072cc35fbe68479f4.mugqic.done
)
estimate_ribosomal_rna_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_10_JOB_ID: bwa_mem_rRNA.SRX377950
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.SRX377950
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.SRX377950.a41cc029396c46bd4dd50764d604ade8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.SRX377950.a41cc029396c46bd4dd50764d604ade8.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/8-cell_rep1/SRX377950 metrics/8-cell_rep1/SRX377950 && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/8-cell_rep1/SRX377950/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:SRX377950	SM:8-cell_rep1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/rrna_bwa_index/Bos_taurus.UMD3.1.Ensembl84.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/8-cell_rep1/SRX377950/SRX377950rRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/8-cell_rep1/SRX377950/SRX377950rRNA.bam \
  -g $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  -o metrics/8-cell_rep1/SRX377950/SRX377950rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.SRX377950.a41cc029396c46bd4dd50764d604ade8.mugqic.done
)
estimate_ribosomal_rna_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_11_JOB_ID: bwa_mem_rRNA.SRX377951
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.SRX377951
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.SRX377951.1301817fc3ed2fd627623403ed2fb2d1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.SRX377951.1301817fc3ed2fd627623403ed2fb2d1.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/8-cell_rep2/SRX377951 metrics/8-cell_rep2/SRX377951 && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/8-cell_rep2/SRX377951/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:SRX377951	SM:8-cell_rep2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/rrna_bwa_index/Bos_taurus.UMD3.1.Ensembl84.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/8-cell_rep2/SRX377951/SRX377951rRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/8-cell_rep2/SRX377951/SRX377951rRNA.bam \
  -g $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  -o metrics/8-cell_rep2/SRX377951/SRX377951rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.SRX377951.1301817fc3ed2fd627623403ed2fb2d1.mugqic.done
)
estimate_ribosomal_rna_11_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_12_JOB_ID: bwa_mem_rRNA.SRX377952
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.SRX377952
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.SRX377952.ee003e966344ab77887eaa6b02278ee5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.SRX377952.ee003e966344ab77887eaa6b02278ee5.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/8-cell_rep3/SRX377952 metrics/8-cell_rep3/SRX377952 && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/8-cell_rep3/SRX377952/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:SRX377952	SM:8-cell_rep3	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/rrna_bwa_index/Bos_taurus.UMD3.1.Ensembl84.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/8-cell_rep3/SRX377952/SRX377952rRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/8-cell_rep3/SRX377952/SRX377952rRNA.bam \
  -g $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  -o metrics/8-cell_rep3/SRX377952/SRX377952rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.SRX377952.ee003e966344ab77887eaa6b02278ee5.mugqic.done
)
estimate_ribosomal_rna_12_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_13_JOB_ID: bwa_mem_rRNA.SRX377953
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.SRX377953
JOB_DEPENDENCIES=$star_1_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.SRX377953.c9e44acff30f3cccd2e315aab3c8a5e1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.SRX377953.c9e44acff30f3cccd2e315aab3c8a5e1.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/16-cell_rep1/SRX377953 metrics/16-cell_rep1/SRX377953 && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/16-cell_rep1/SRX377953/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:SRX377953	SM:16-cell_rep1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/rrna_bwa_index/Bos_taurus.UMD3.1.Ensembl84.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/16-cell_rep1/SRX377953/SRX377953rRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/16-cell_rep1/SRX377953/SRX377953rRNA.bam \
  -g $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  -o metrics/16-cell_rep1/SRX377953/SRX377953rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.SRX377953.c9e44acff30f3cccd2e315aab3c8a5e1.mugqic.done
)
estimate_ribosomal_rna_13_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_14_JOB_ID: bwa_mem_rRNA.SRX377954
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.SRX377954
JOB_DEPENDENCIES=$star_2_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.SRX377954.4335ede72d2f211a4ac3e1c0347ca30f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.SRX377954.4335ede72d2f211a4ac3e1c0347ca30f.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/16-cell_rep2/SRX377954 metrics/16-cell_rep2/SRX377954 && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/16-cell_rep2/SRX377954/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:SRX377954	SM:16-cell_rep2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/rrna_bwa_index/Bos_taurus.UMD3.1.Ensembl84.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/16-cell_rep2/SRX377954/SRX377954rRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/16-cell_rep2/SRX377954/SRX377954rRNA.bam \
  -g $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  -o metrics/16-cell_rep2/SRX377954/SRX377954rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.SRX377954.4335ede72d2f211a4ac3e1c0347ca30f.mugqic.done
)
estimate_ribosomal_rna_14_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_15_JOB_ID: bwa_mem_rRNA.SRX377955
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.SRX377955
JOB_DEPENDENCIES=$star_3_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.SRX377955.a80502953d8e2cdb53f0b73c38b3ca7d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.SRX377955.a80502953d8e2cdb53f0b73c38b3ca7d.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/16-cell_rep3/SRX377955 metrics/16-cell_rep3/SRX377955 && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/16-cell_rep3/SRX377955/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:SRX377955	SM:16-cell_rep3	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/rrna_bwa_index/Bos_taurus.UMD3.1.Ensembl84.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/16-cell_rep3/SRX377955/SRX377955rRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/16-cell_rep3/SRX377955/SRX377955rRNA.bam \
  -g $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  -o metrics/16-cell_rep3/SRX377955/SRX377955rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.SRX377955.a80502953d8e2cdb53f0b73c38b3ca7d.mugqic.done
)
estimate_ribosomal_rna_15_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_16_JOB_ID: bwa_mem_rRNA.SRX377956
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.SRX377956
JOB_DEPENDENCIES=$star_4_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.SRX377956.41fa33f9c6a7f40dd8a343618a444e4a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.SRX377956.41fa33f9c6a7f40dd8a343618a444e4a.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Blastocyst_rep1/SRX377956 metrics/Blastocyst_rep1/SRX377956 && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Blastocyst_rep1/SRX377956/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:SRX377956	SM:Blastocyst_rep1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/rrna_bwa_index/Bos_taurus.UMD3.1.Ensembl84.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Blastocyst_rep1/SRX377956/SRX377956rRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Blastocyst_rep1/SRX377956/SRX377956rRNA.bam \
  -g $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  -o metrics/Blastocyst_rep1/SRX377956/SRX377956rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.SRX377956.41fa33f9c6a7f40dd8a343618a444e4a.mugqic.done
)
estimate_ribosomal_rna_16_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_17_JOB_ID: bwa_mem_rRNA.SRX377957
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.SRX377957
JOB_DEPENDENCIES=$star_5_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.SRX377957.f87f9f929714b434d4bf397e464bb9b3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.SRX377957.f87f9f929714b434d4bf397e464bb9b3.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Blastocyst_rep2/SRX377957 metrics/Blastocyst_rep2/SRX377957 && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Blastocyst_rep2/SRX377957/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:SRX377957	SM:Blastocyst_rep2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/rrna_bwa_index/Bos_taurus.UMD3.1.Ensembl84.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Blastocyst_rep2/SRX377957/SRX377957rRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Blastocyst_rep2/SRX377957/SRX377957rRNA.bam \
  -g $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  -o metrics/Blastocyst_rep2/SRX377957/SRX377957rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.SRX377957.f87f9f929714b434d4bf397e464bb9b3.mugqic.done
)
estimate_ribosomal_rna_17_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_18_JOB_ID: bwa_mem_rRNA.SRX377958
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.SRX377958
JOB_DEPENDENCIES=$star_6_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.SRX377958.2e5f4e4350ca625cd6f0b7d80eeffbd2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.SRX377958.2e5f4e4350ca625cd6f0b7d80eeffbd2.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Blastocyst_rep3/SRX377958 metrics/Blastocyst_rep3/SRX377958 && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Blastocyst_rep3/SRX377958/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:SRX377958	SM:Blastocyst_rep3	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/rrna_bwa_index/Bos_taurus.UMD3.1.Ensembl84.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Blastocyst_rep3/SRX377958/SRX377958rRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Blastocyst_rep3/SRX377958/SRX377958rRNA.bam \
  -g $MUGQIC_INSTALL_HOME_DEV/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  -o metrics/Blastocyst_rep3/SRX377958/SRX377958rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.SRX377958.2e5f4e4350ca625cd6f0b7d80eeffbd2.mugqic.done
)
estimate_ribosomal_rna_18_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: bam_hard_clip
#-------------------------------------------------------------------------------
STEP=bam_hard_clip
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_1_JOB_ID: tuxedo_hard_clip.GV-oocyte_rep1
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.GV-oocyte_rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.GV-oocyte_rep1.57a772b9813fa05a718951f30153aa1b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.GV-oocyte_rep1.57a772b9813fa05a718951f30153aa1b.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/GV-oocyte_rep1/GV-oocyte_rep1.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/GV-oocyte_rep1/GV-oocyte_rep1.sorted.mdup.hardClip.bam
tuxedo_hard_clip.GV-oocyte_rep1.57a772b9813fa05a718951f30153aa1b.mugqic.done
)
bam_hard_clip_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_2_JOB_ID: tuxedo_hard_clip.GV-oocyte_rep2
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.GV-oocyte_rep2
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.GV-oocyte_rep2.65544fc734ffb222a3c2bcfab6a06c62.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.GV-oocyte_rep2.65544fc734ffb222a3c2bcfab6a06c62.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/GV-oocyte_rep2/GV-oocyte_rep2.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/GV-oocyte_rep2/GV-oocyte_rep2.sorted.mdup.hardClip.bam
tuxedo_hard_clip.GV-oocyte_rep2.65544fc734ffb222a3c2bcfab6a06c62.mugqic.done
)
bam_hard_clip_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_3_JOB_ID: tuxedo_hard_clip.GV-oocyte_rep3
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.GV-oocyte_rep3
JOB_DEPENDENCIES=$picard_mark_duplicates_3_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.GV-oocyte_rep3.22a50c65fd2e37712a18d2288aa075b8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.GV-oocyte_rep3.22a50c65fd2e37712a18d2288aa075b8.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/GV-oocyte_rep3/GV-oocyte_rep3.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/GV-oocyte_rep3/GV-oocyte_rep3.sorted.mdup.hardClip.bam
tuxedo_hard_clip.GV-oocyte_rep3.22a50c65fd2e37712a18d2288aa075b8.mugqic.done
)
bam_hard_clip_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_4_JOB_ID: tuxedo_hard_clip.MII-oocyte_rep1
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.MII-oocyte_rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.MII-oocyte_rep1.d18b83550d5db28594873aa3eef1666d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.MII-oocyte_rep1.d18b83550d5db28594873aa3eef1666d.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/MII-oocyte_rep1/MII-oocyte_rep1.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/MII-oocyte_rep1/MII-oocyte_rep1.sorted.mdup.hardClip.bam
tuxedo_hard_clip.MII-oocyte_rep1.d18b83550d5db28594873aa3eef1666d.mugqic.done
)
bam_hard_clip_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_5_JOB_ID: tuxedo_hard_clip.MII-oocyte_rep2
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.MII-oocyte_rep2
JOB_DEPENDENCIES=$picard_mark_duplicates_5_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.MII-oocyte_rep2.18e2c20c7d95933fb90ff152a90c1b50.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.MII-oocyte_rep2.18e2c20c7d95933fb90ff152a90c1b50.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/MII-oocyte_rep2/MII-oocyte_rep2.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/MII-oocyte_rep2/MII-oocyte_rep2.sorted.mdup.hardClip.bam
tuxedo_hard_clip.MII-oocyte_rep2.18e2c20c7d95933fb90ff152a90c1b50.mugqic.done
)
bam_hard_clip_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_6_JOB_ID: tuxedo_hard_clip.MII-oocyte_rep3
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.MII-oocyte_rep3
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.MII-oocyte_rep3.f0e27d0505e6797e2311ef150c325710.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.MII-oocyte_rep3.f0e27d0505e6797e2311ef150c325710.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/MII-oocyte_rep3/MII-oocyte_rep3.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/MII-oocyte_rep3/MII-oocyte_rep3.sorted.mdup.hardClip.bam
tuxedo_hard_clip.MII-oocyte_rep3.f0e27d0505e6797e2311ef150c325710.mugqic.done
)
bam_hard_clip_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_7_JOB_ID: tuxedo_hard_clip.4-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.4-cell_rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_7_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.4-cell_rep1.aa53b79ad357e4ad6faea4a1c3af32b4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.4-cell_rep1.aa53b79ad357e4ad6faea4a1c3af32b4.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/4-cell_rep1/4-cell_rep1.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/4-cell_rep1/4-cell_rep1.sorted.mdup.hardClip.bam
tuxedo_hard_clip.4-cell_rep1.aa53b79ad357e4ad6faea4a1c3af32b4.mugqic.done
)
bam_hard_clip_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_8_JOB_ID: tuxedo_hard_clip.4-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.4-cell_rep2
JOB_DEPENDENCIES=$picard_mark_duplicates_8_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.4-cell_rep2.a45d073b08b883268b3f162e0cee27c6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.4-cell_rep2.a45d073b08b883268b3f162e0cee27c6.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/4-cell_rep2/4-cell_rep2.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/4-cell_rep2/4-cell_rep2.sorted.mdup.hardClip.bam
tuxedo_hard_clip.4-cell_rep2.a45d073b08b883268b3f162e0cee27c6.mugqic.done
)
bam_hard_clip_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_9_JOB_ID: tuxedo_hard_clip.4-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.4-cell_rep3
JOB_DEPENDENCIES=$picard_mark_duplicates_9_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.4-cell_rep3.137693f81ae630e16a13faf1801c7daa.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.4-cell_rep3.137693f81ae630e16a13faf1801c7daa.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/4-cell_rep3/4-cell_rep3.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/4-cell_rep3/4-cell_rep3.sorted.mdup.hardClip.bam
tuxedo_hard_clip.4-cell_rep3.137693f81ae630e16a13faf1801c7daa.mugqic.done
)
bam_hard_clip_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_10_JOB_ID: tuxedo_hard_clip.8-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.8-cell_rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_10_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.8-cell_rep1.0feb0fd306810c778d6f9489a2682f53.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.8-cell_rep1.0feb0fd306810c778d6f9489a2682f53.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/8-cell_rep1/8-cell_rep1.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/8-cell_rep1/8-cell_rep1.sorted.mdup.hardClip.bam
tuxedo_hard_clip.8-cell_rep1.0feb0fd306810c778d6f9489a2682f53.mugqic.done
)
bam_hard_clip_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_11_JOB_ID: tuxedo_hard_clip.8-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.8-cell_rep2
JOB_DEPENDENCIES=$picard_mark_duplicates_11_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.8-cell_rep2.73876d410ba0ab25d3bca9279158b467.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.8-cell_rep2.73876d410ba0ab25d3bca9279158b467.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/8-cell_rep2/8-cell_rep2.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/8-cell_rep2/8-cell_rep2.sorted.mdup.hardClip.bam
tuxedo_hard_clip.8-cell_rep2.73876d410ba0ab25d3bca9279158b467.mugqic.done
)
bam_hard_clip_11_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_12_JOB_ID: tuxedo_hard_clip.8-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.8-cell_rep3
JOB_DEPENDENCIES=$picard_mark_duplicates_12_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.8-cell_rep3.573aeed0758ea88bcdd420a94936a17d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.8-cell_rep3.573aeed0758ea88bcdd420a94936a17d.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/8-cell_rep3/8-cell_rep3.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/8-cell_rep3/8-cell_rep3.sorted.mdup.hardClip.bam
tuxedo_hard_clip.8-cell_rep3.573aeed0758ea88bcdd420a94936a17d.mugqic.done
)
bam_hard_clip_12_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_13_JOB_ID: tuxedo_hard_clip.16-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.16-cell_rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_13_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.16-cell_rep1.f145b248fdabc68499a68702fe6d27f5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.16-cell_rep1.f145b248fdabc68499a68702fe6d27f5.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/16-cell_rep1/16-cell_rep1.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/16-cell_rep1/16-cell_rep1.sorted.mdup.hardClip.bam
tuxedo_hard_clip.16-cell_rep1.f145b248fdabc68499a68702fe6d27f5.mugqic.done
)
bam_hard_clip_13_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_14_JOB_ID: tuxedo_hard_clip.16-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.16-cell_rep2
JOB_DEPENDENCIES=$picard_mark_duplicates_14_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.16-cell_rep2.be72fe12d1ec84d648c39d137d10be2e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.16-cell_rep2.be72fe12d1ec84d648c39d137d10be2e.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/16-cell_rep2/16-cell_rep2.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/16-cell_rep2/16-cell_rep2.sorted.mdup.hardClip.bam
tuxedo_hard_clip.16-cell_rep2.be72fe12d1ec84d648c39d137d10be2e.mugqic.done
)
bam_hard_clip_14_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_15_JOB_ID: tuxedo_hard_clip.16-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.16-cell_rep3
JOB_DEPENDENCIES=$picard_mark_duplicates_15_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.16-cell_rep3.0c0e11d29bdf69c8d1f2a930bb89751b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.16-cell_rep3.0c0e11d29bdf69c8d1f2a930bb89751b.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/16-cell_rep3/16-cell_rep3.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/16-cell_rep3/16-cell_rep3.sorted.mdup.hardClip.bam
tuxedo_hard_clip.16-cell_rep3.0c0e11d29bdf69c8d1f2a930bb89751b.mugqic.done
)
bam_hard_clip_15_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_16_JOB_ID: tuxedo_hard_clip.Blastocyst_rep1
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.Blastocyst_rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_16_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.Blastocyst_rep1.c294b11029ece95017a6b542cb98be29.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.Blastocyst_rep1.c294b11029ece95017a6b542cb98be29.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/Blastocyst_rep1/Blastocyst_rep1.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/Blastocyst_rep1/Blastocyst_rep1.sorted.mdup.hardClip.bam
tuxedo_hard_clip.Blastocyst_rep1.c294b11029ece95017a6b542cb98be29.mugqic.done
)
bam_hard_clip_16_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_17_JOB_ID: tuxedo_hard_clip.Blastocyst_rep2
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.Blastocyst_rep2
JOB_DEPENDENCIES=$picard_mark_duplicates_17_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.Blastocyst_rep2.658ad5c788ea1b411ab124c2c3298af1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.Blastocyst_rep2.658ad5c788ea1b411ab124c2c3298af1.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/Blastocyst_rep2/Blastocyst_rep2.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/Blastocyst_rep2/Blastocyst_rep2.sorted.mdup.hardClip.bam
tuxedo_hard_clip.Blastocyst_rep2.658ad5c788ea1b411ab124c2c3298af1.mugqic.done
)
bam_hard_clip_17_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: bam_hard_clip_18_JOB_ID: tuxedo_hard_clip.Blastocyst_rep3
#-------------------------------------------------------------------------------
JOB_NAME=tuxedo_hard_clip.Blastocyst_rep3
JOB_DEPENDENCIES=$picard_mark_duplicates_18_JOB_ID
JOB_DONE=job_output/bam_hard_clip/tuxedo_hard_clip.Blastocyst_rep3.cc9764a3388bc592285cb02b26836311.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'tuxedo_hard_clip.Blastocyst_rep3.cc9764a3388bc592285cb02b26836311.mugqic.done'
module load mugqic/samtools/1.4.1 && \
samtools view -h \
  alignment/Blastocyst_rep3/Blastocyst_rep3.sorted.mdup.bam | \
awk 'BEGIN {OFS="\t"} {if (substr($1,1,1)=="@") {print;next}; split($6,C,/[0-9]*/); split($6,L,/[SMDIN]/); if (C[2]=="S") {$10=substr($10,L[1]+1); $11=substr($11,L[1]+1)}; if (C[length(C)]=="S") {L1=length($10)-L[length(L)-1]; $10=substr($10,1,L1); $11=substr($11,1,L1); }; gsub(/[0-9]*S/,"",$6); print}'  | \
samtools view -hbS \
  - \
  > alignment/Blastocyst_rep3/Blastocyst_rep3.sorted.mdup.hardClip.bam
tuxedo_hard_clip.Blastocyst_rep3.cc9764a3388bc592285cb02b26836311.mugqic.done
)
bam_hard_clip_18_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"bam_hard_clip\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$bam_hard_clip_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: rnaseqc
#-------------------------------------------------------------------------------
STEP=rnaseqc
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: rnaseqc_1_JOB_ID: rnaseqc
#-------------------------------------------------------------------------------
JOB_NAME=rnaseqc
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID:$picard_mark_duplicates_2_JOB_ID:$picard_mark_duplicates_3_JOB_ID:$picard_mark_duplicates_4_JOB_ID:$picard_mark_duplicates_5_JOB_ID:$picard_mark_duplicates_6_JOB_ID:$picard_mark_duplicates_7_JOB_ID:$picard_mark_duplicates_8_JOB_ID:$picard_mark_duplicates_9_JOB_ID:$picard_mark_duplicates_10_JOB_ID:$picard_mark_duplicates_11_JOB_ID:$picard_mark_duplicates_12_JOB_ID:$picard_mark_duplicates_13_JOB_ID:$picard_mark_duplicates_14_JOB_ID:$picard_mark_duplicates_15_JOB_ID:$picard_mark_duplicates_16_JOB_ID:$picard_mark_duplicates_17_JOB_ID:$picard_mark_duplicates_18_JOB_ID
JOB_DONE=job_output/rnaseqc/rnaseqc.e1eb99e915e3285b8460a5b30e62998c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'rnaseqc.e1eb99e915e3285b8460a5b30e62998c.mugqic.done'
module load mugqic/java/openjdk-jdk1.7.0_60 mugqic/bwa/0.7.15 mugqic/rnaseqc/1.1.8 && \
mkdir -p metrics/rnaseqRep && \
echo "Sample	BamFile	Note
GV-oocyte_rep1	alignment/GV-oocyte_rep1/GV-oocyte_rep1.sorted.mdup.bam	RNAseq
GV-oocyte_rep2	alignment/GV-oocyte_rep2/GV-oocyte_rep2.sorted.mdup.bam	RNAseq
GV-oocyte_rep3	alignment/GV-oocyte_rep3/GV-oocyte_rep3.sorted.mdup.bam	RNAseq
MII-oocyte_rep1	alignment/MII-oocyte_rep1/MII-oocyte_rep1.sorted.mdup.bam	RNAseq
MII-oocyte_rep2	alignment/MII-oocyte_rep2/MII-oocyte_rep2.sorted.mdup.bam	RNAseq
MII-oocyte_rep3	alignment/MII-oocyte_rep3/MII-oocyte_rep3.sorted.mdup.bam	RNAseq
4-cell_rep1	alignment/4-cell_rep1/4-cell_rep1.sorted.mdup.bam	RNAseq
4-cell_rep2	alignment/4-cell_rep2/4-cell_rep2.sorted.mdup.bam	RNAseq
4-cell_rep3	alignment/4-cell_rep3/4-cell_rep3.sorted.mdup.bam	RNAseq
8-cell_rep1	alignment/8-cell_rep1/8-cell_rep1.sorted.mdup.bam	RNAseq
8-cell_rep2	alignment/8-cell_rep2/8-cell_rep2.sorted.mdup.bam	RNAseq
8-cell_rep3	alignment/8-cell_rep3/8-cell_rep3.sorted.mdup.bam	RNAseq
16-cell_rep1	alignment/16-cell_rep1/16-cell_rep1.sorted.mdup.bam	RNAseq
16-cell_rep2	alignment/16-cell_rep2/16-cell_rep2.sorted.mdup.bam	RNAseq
16-cell_rep3	alignment/16-cell_rep3/16-cell_rep3.sorted.mdup.bam	RNAseq
Blastocyst_rep1	alignment/Blastocyst_rep1/Blastocyst_rep1.sorted.mdup.bam	RNAseq
Blastocyst_rep2	alignment/Blastocyst_rep2/Blastocyst_rep2.sorted.mdup.bam	RNAseq
Blastocyst_rep3	alignment/Blastocyst_rep3/Blastocyst_rep3.sorted.mdup.bam	RNAseq" \
  > alignment/rnaseqc.samples.txt && \
touch dummy_rRNA.fa && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $RNASEQC_JAR \
  -n 1000 \
  -o metrics/rnaseqRep \
  -r /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
  -s alignment/rnaseqc.samples.txt \
  -t /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.transcript_id.gtf \
  -ttype 2 \
  -singleEnd\
  -BWArRNA dummy_rRNA.fa && \
zip -r metrics/rnaseqRep.zip metrics/rnaseqRep
rnaseqc.e1eb99e915e3285b8460a5b30e62998c.mugqic.done
)
rnaseqc_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"rnaseqc\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"rnaseqc\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=72:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$rnaseqc_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: rnaseqc_2_JOB_ID: rnaseqc_report
#-------------------------------------------------------------------------------
JOB_NAME=rnaseqc_report
JOB_DEPENDENCIES=$rnaseqc_1_JOB_ID
JOB_DONE=job_output/rnaseqc/rnaseqc_report.67821e702049231d38161c5db01ef648.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'rnaseqc_report.67821e702049231d38161c5db01ef648.mugqic.done'
module load mugqic/python/2.7.13 mugqic/pandoc/1.15.2 && \
mkdir -p report && \
cp metrics/rnaseqRep.zip report/reportRNAseqQC.zip && \
python -c 'import csv; csv_in = csv.DictReader(open("metrics/rnaseqRep/metrics.tsv"), delimiter="	")
print "	".join(["Sample", "Aligned Reads", "Alternative Alignments", "%", "rRNA Reads", "Coverage", "Exonic Rate", "Genes"])
print "\n".join(["	".join([
    line["Sample"],
    line["Mapped"],
    line["Alternative Aligments"],
    str(float(line["Alternative Aligments"]) / float(line["Mapped"]) * 100),
    line["rRNA"],
    line["Mean Per Base Cov."],
    line["Exonic Rate"],
    line["Genes Detected"]
]) for line in csv_in])' \
  > report/trimAlignmentTable.tsv.tmp && \
if [[ -f metrics/trimSampleTable.tsv ]]
then
  awk -F"	" 'FNR==NR{raw_reads[$1]=$2; surviving_reads[$1]=$3; surviving_pct[$1]=$4; next}{OFS="	"; if ($2=="Aligned Reads"){surviving_pct[$1]="%"; aligned_pct="%"; rrna_pct="%"} else {aligned_pct=($2 / surviving_reads[$1] * 100); rrna_pct=($5 / surviving_reads[$1] * 100)}; printf $1"	"raw_reads[$1]"	"surviving_reads[$1]"	"surviving_pct[$1]"	"$2"	"aligned_pct"	"$3"	"$4"	"$5"	"rrna_pct; for (i = 6; i<= NF; i++) {printf "	"$i}; print ""}' \
  metrics/trimSampleTable.tsv \
  report/trimAlignmentTable.tsv.tmp \
  > report/trimAlignmentTable.tsv
else
  cp report/trimAlignmentTable.tsv.tmp report/trimAlignmentTable.tsv
fi && \
rm report/trimAlignmentTable.tsv.tmp && \
trim_alignment_table_md=`if [[ -f metrics/trimSampleTable.tsv ]] ; then cut -f1-13 report/trimAlignmentTable.tsv | LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:"} else {print $1, sprintf("%\47d", $2), sprintf("%\47d", $3), sprintf("%.1f", $4), sprintf("%\47d", $5), sprintf("%.1f", $6), sprintf("%\47d", $7), sprintf("%.1f", $8), sprintf("%\47d", $9), sprintf("%.1f", $10), sprintf("%.2f", $11), sprintf("%.2f", $12), sprintf("%\47d", $13)}}' ; else cat report/trimAlignmentTable.tsv | LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----:|-----:|-----:|-----:|-----:|-----:|-----:"} else {print $1, sprintf("%\47d", $2), sprintf("%\47d", $3), sprintf("%.1f", $4), sprintf("%\47d", $5), sprintf("%.2f", $6), sprintf("%.2f", $7), $8}}' ; fi`
pandoc \
  /home/efournie/genpipes/bfx/report/RnaSeq.rnaseqc.md \
  --template /home/efournie/genpipes/bfx/report/RnaSeq.rnaseqc.md \
  --variable trim_alignment_table="$trim_alignment_table_md" \
  --to markdown \
  > report/RnaSeq.rnaseqc.md
rnaseqc_report.67821e702049231d38161c5db01ef648.mugqic.done
)
rnaseqc_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$rnaseqc_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: wiggle
#-------------------------------------------------------------------------------
STEP=wiggle
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: wiggle_1_JOB_ID: bed_graph.GV-oocyte_rep1
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.GV-oocyte_rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.GV-oocyte_rep1.988d7b5802c091e0b8d0e77e6022e0d5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.GV-oocyte_rep1.988d7b5802c091e0b8d0e77e6022e0d5.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/GV-oocyte_rep1  && \
nmblines=$(samtools view -F 256 alignment/GV-oocyte_rep1/GV-oocyte_rep1.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/GV-oocyte_rep1/GV-oocyte_rep1.sorted.mdup.bam \
  -g /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  > tracks/GV-oocyte_rep1/GV-oocyte_rep1.bedGraph
bed_graph.GV-oocyte_rep1.988d7b5802c091e0b8d0e77e6022e0d5.mugqic.done
)
wiggle_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_2_JOB_ID: wiggle.GV-oocyte_rep1
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.GV-oocyte_rep1
JOB_DEPENDENCIES=$wiggle_1_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.GV-oocyte_rep1.4347d13bd11170b4502066c8c0384165.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.GV-oocyte_rep1.4347d13bd11170b4502066c8c0384165.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/GV-oocyte_rep1/GV-oocyte_rep1.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/GV-oocyte_rep1/GV-oocyte_rep1.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/GV-oocyte_rep1/GV-oocyte_rep1.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  tracks/bigWig/GV-oocyte_rep1.bw
wiggle.GV-oocyte_rep1.4347d13bd11170b4502066c8c0384165.mugqic.done
)
wiggle_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_3_JOB_ID: bed_graph.GV-oocyte_rep2
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.GV-oocyte_rep2
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.GV-oocyte_rep2.cc9c74bf3c6db01ce786e0a097fc3752.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.GV-oocyte_rep2.cc9c74bf3c6db01ce786e0a097fc3752.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/GV-oocyte_rep2  && \
nmblines=$(samtools view -F 256 alignment/GV-oocyte_rep2/GV-oocyte_rep2.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/GV-oocyte_rep2/GV-oocyte_rep2.sorted.mdup.bam \
  -g /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  > tracks/GV-oocyte_rep2/GV-oocyte_rep2.bedGraph
bed_graph.GV-oocyte_rep2.cc9c74bf3c6db01ce786e0a097fc3752.mugqic.done
)
wiggle_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_4_JOB_ID: wiggle.GV-oocyte_rep2
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.GV-oocyte_rep2
JOB_DEPENDENCIES=$wiggle_3_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.GV-oocyte_rep2.3e7fe854c8da2011d85a113abd89ed45.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.GV-oocyte_rep2.3e7fe854c8da2011d85a113abd89ed45.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/GV-oocyte_rep2/GV-oocyte_rep2.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/GV-oocyte_rep2/GV-oocyte_rep2.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/GV-oocyte_rep2/GV-oocyte_rep2.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  tracks/bigWig/GV-oocyte_rep2.bw
wiggle.GV-oocyte_rep2.3e7fe854c8da2011d85a113abd89ed45.mugqic.done
)
wiggle_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_5_JOB_ID: bed_graph.GV-oocyte_rep3
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.GV-oocyte_rep3
JOB_DEPENDENCIES=$picard_mark_duplicates_3_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.GV-oocyte_rep3.4b70fb4f1eddc273c664d1c597f7a616.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.GV-oocyte_rep3.4b70fb4f1eddc273c664d1c597f7a616.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/GV-oocyte_rep3  && \
nmblines=$(samtools view -F 256 alignment/GV-oocyte_rep3/GV-oocyte_rep3.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/GV-oocyte_rep3/GV-oocyte_rep3.sorted.mdup.bam \
  -g /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  > tracks/GV-oocyte_rep3/GV-oocyte_rep3.bedGraph
bed_graph.GV-oocyte_rep3.4b70fb4f1eddc273c664d1c597f7a616.mugqic.done
)
wiggle_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_6_JOB_ID: wiggle.GV-oocyte_rep3
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.GV-oocyte_rep3
JOB_DEPENDENCIES=$wiggle_5_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.GV-oocyte_rep3.ad921f5354e0d7d4e15033a88bef2d6b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.GV-oocyte_rep3.ad921f5354e0d7d4e15033a88bef2d6b.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/GV-oocyte_rep3/GV-oocyte_rep3.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/GV-oocyte_rep3/GV-oocyte_rep3.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/GV-oocyte_rep3/GV-oocyte_rep3.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  tracks/bigWig/GV-oocyte_rep3.bw
wiggle.GV-oocyte_rep3.ad921f5354e0d7d4e15033a88bef2d6b.mugqic.done
)
wiggle_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_7_JOB_ID: bed_graph.MII-oocyte_rep1
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.MII-oocyte_rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.MII-oocyte_rep1.12717a1feab0231ce5cd8a44bff46693.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.MII-oocyte_rep1.12717a1feab0231ce5cd8a44bff46693.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/MII-oocyte_rep1  && \
nmblines=$(samtools view -F 256 alignment/MII-oocyte_rep1/MII-oocyte_rep1.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/MII-oocyte_rep1/MII-oocyte_rep1.sorted.mdup.bam \
  -g /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  > tracks/MII-oocyte_rep1/MII-oocyte_rep1.bedGraph
bed_graph.MII-oocyte_rep1.12717a1feab0231ce5cd8a44bff46693.mugqic.done
)
wiggle_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_8_JOB_ID: wiggle.MII-oocyte_rep1
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.MII-oocyte_rep1
JOB_DEPENDENCIES=$wiggle_7_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.MII-oocyte_rep1.9613799900d572b650d9d44f3c7349f9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.MII-oocyte_rep1.9613799900d572b650d9d44f3c7349f9.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/MII-oocyte_rep1/MII-oocyte_rep1.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/MII-oocyte_rep1/MII-oocyte_rep1.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/MII-oocyte_rep1/MII-oocyte_rep1.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  tracks/bigWig/MII-oocyte_rep1.bw
wiggle.MII-oocyte_rep1.9613799900d572b650d9d44f3c7349f9.mugqic.done
)
wiggle_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_9_JOB_ID: bed_graph.MII-oocyte_rep2
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.MII-oocyte_rep2
JOB_DEPENDENCIES=$picard_mark_duplicates_5_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.MII-oocyte_rep2.fc5b05fa2c49764b2a352466123e7702.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.MII-oocyte_rep2.fc5b05fa2c49764b2a352466123e7702.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/MII-oocyte_rep2  && \
nmblines=$(samtools view -F 256 alignment/MII-oocyte_rep2/MII-oocyte_rep2.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/MII-oocyte_rep2/MII-oocyte_rep2.sorted.mdup.bam \
  -g /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  > tracks/MII-oocyte_rep2/MII-oocyte_rep2.bedGraph
bed_graph.MII-oocyte_rep2.fc5b05fa2c49764b2a352466123e7702.mugqic.done
)
wiggle_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_10_JOB_ID: wiggle.MII-oocyte_rep2
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.MII-oocyte_rep2
JOB_DEPENDENCIES=$wiggle_9_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.MII-oocyte_rep2.3e7fea642025c68df562586a60b13de9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.MII-oocyte_rep2.3e7fea642025c68df562586a60b13de9.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/MII-oocyte_rep2/MII-oocyte_rep2.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/MII-oocyte_rep2/MII-oocyte_rep2.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/MII-oocyte_rep2/MII-oocyte_rep2.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  tracks/bigWig/MII-oocyte_rep2.bw
wiggle.MII-oocyte_rep2.3e7fea642025c68df562586a60b13de9.mugqic.done
)
wiggle_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_11_JOB_ID: bed_graph.MII-oocyte_rep3
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.MII-oocyte_rep3
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.MII-oocyte_rep3.28fd073a5fdea20d083acfe33456f46e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.MII-oocyte_rep3.28fd073a5fdea20d083acfe33456f46e.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/MII-oocyte_rep3  && \
nmblines=$(samtools view -F 256 alignment/MII-oocyte_rep3/MII-oocyte_rep3.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/MII-oocyte_rep3/MII-oocyte_rep3.sorted.mdup.bam \
  -g /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  > tracks/MII-oocyte_rep3/MII-oocyte_rep3.bedGraph
bed_graph.MII-oocyte_rep3.28fd073a5fdea20d083acfe33456f46e.mugqic.done
)
wiggle_11_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_12_JOB_ID: wiggle.MII-oocyte_rep3
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.MII-oocyte_rep3
JOB_DEPENDENCIES=$wiggle_11_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.MII-oocyte_rep3.2ddb5119a9459cf79b76dc312656e799.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.MII-oocyte_rep3.2ddb5119a9459cf79b76dc312656e799.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/MII-oocyte_rep3/MII-oocyte_rep3.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/MII-oocyte_rep3/MII-oocyte_rep3.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/MII-oocyte_rep3/MII-oocyte_rep3.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  tracks/bigWig/MII-oocyte_rep3.bw
wiggle.MII-oocyte_rep3.2ddb5119a9459cf79b76dc312656e799.mugqic.done
)
wiggle_12_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_13_JOB_ID: bed_graph.4-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.4-cell_rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_7_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.4-cell_rep1.72a8cb24c58ab9594276baf84c8ba50f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.4-cell_rep1.72a8cb24c58ab9594276baf84c8ba50f.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/4-cell_rep1  && \
nmblines=$(samtools view -F 256 alignment/4-cell_rep1/4-cell_rep1.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/4-cell_rep1/4-cell_rep1.sorted.mdup.bam \
  -g /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  > tracks/4-cell_rep1/4-cell_rep1.bedGraph
bed_graph.4-cell_rep1.72a8cb24c58ab9594276baf84c8ba50f.mugqic.done
)
wiggle_13_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_14_JOB_ID: wiggle.4-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.4-cell_rep1
JOB_DEPENDENCIES=$wiggle_13_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.4-cell_rep1.0e61e63c8f11f9957ebcddd5f40ff2c5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.4-cell_rep1.0e61e63c8f11f9957ebcddd5f40ff2c5.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/4-cell_rep1/4-cell_rep1.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/4-cell_rep1/4-cell_rep1.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/4-cell_rep1/4-cell_rep1.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  tracks/bigWig/4-cell_rep1.bw
wiggle.4-cell_rep1.0e61e63c8f11f9957ebcddd5f40ff2c5.mugqic.done
)
wiggle_14_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_15_JOB_ID: bed_graph.4-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.4-cell_rep2
JOB_DEPENDENCIES=$picard_mark_duplicates_8_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.4-cell_rep2.1fa71522afe542b836bdd30331e52fe2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.4-cell_rep2.1fa71522afe542b836bdd30331e52fe2.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/4-cell_rep2  && \
nmblines=$(samtools view -F 256 alignment/4-cell_rep2/4-cell_rep2.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/4-cell_rep2/4-cell_rep2.sorted.mdup.bam \
  -g /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  > tracks/4-cell_rep2/4-cell_rep2.bedGraph
bed_graph.4-cell_rep2.1fa71522afe542b836bdd30331e52fe2.mugqic.done
)
wiggle_15_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_16_JOB_ID: wiggle.4-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.4-cell_rep2
JOB_DEPENDENCIES=$wiggle_15_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.4-cell_rep2.4f683abb25c0cda4be30a038678c6342.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.4-cell_rep2.4f683abb25c0cda4be30a038678c6342.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/4-cell_rep2/4-cell_rep2.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/4-cell_rep2/4-cell_rep2.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/4-cell_rep2/4-cell_rep2.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  tracks/bigWig/4-cell_rep2.bw
wiggle.4-cell_rep2.4f683abb25c0cda4be30a038678c6342.mugqic.done
)
wiggle_16_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_17_JOB_ID: bed_graph.4-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.4-cell_rep3
JOB_DEPENDENCIES=$picard_mark_duplicates_9_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.4-cell_rep3.c667bae77eb65429474bc03008664b69.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.4-cell_rep3.c667bae77eb65429474bc03008664b69.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/4-cell_rep3  && \
nmblines=$(samtools view -F 256 alignment/4-cell_rep3/4-cell_rep3.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/4-cell_rep3/4-cell_rep3.sorted.mdup.bam \
  -g /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  > tracks/4-cell_rep3/4-cell_rep3.bedGraph
bed_graph.4-cell_rep3.c667bae77eb65429474bc03008664b69.mugqic.done
)
wiggle_17_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_18_JOB_ID: wiggle.4-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.4-cell_rep3
JOB_DEPENDENCIES=$wiggle_17_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.4-cell_rep3.3d3d5f3c5a3499da83c7e9e8762ad6a0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.4-cell_rep3.3d3d5f3c5a3499da83c7e9e8762ad6a0.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/4-cell_rep3/4-cell_rep3.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/4-cell_rep3/4-cell_rep3.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/4-cell_rep3/4-cell_rep3.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  tracks/bigWig/4-cell_rep3.bw
wiggle.4-cell_rep3.3d3d5f3c5a3499da83c7e9e8762ad6a0.mugqic.done
)
wiggle_18_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_19_JOB_ID: bed_graph.8-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.8-cell_rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_10_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.8-cell_rep1.8d74c718a1dc5aded6ecef4bd35efb1b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.8-cell_rep1.8d74c718a1dc5aded6ecef4bd35efb1b.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/8-cell_rep1  && \
nmblines=$(samtools view -F 256 alignment/8-cell_rep1/8-cell_rep1.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/8-cell_rep1/8-cell_rep1.sorted.mdup.bam \
  -g /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  > tracks/8-cell_rep1/8-cell_rep1.bedGraph
bed_graph.8-cell_rep1.8d74c718a1dc5aded6ecef4bd35efb1b.mugqic.done
)
wiggle_19_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_20_JOB_ID: wiggle.8-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.8-cell_rep1
JOB_DEPENDENCIES=$wiggle_19_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.8-cell_rep1.f60287d5ff9f8e8003f3a3b03aec1433.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.8-cell_rep1.f60287d5ff9f8e8003f3a3b03aec1433.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/8-cell_rep1/8-cell_rep1.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/8-cell_rep1/8-cell_rep1.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/8-cell_rep1/8-cell_rep1.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  tracks/bigWig/8-cell_rep1.bw
wiggle.8-cell_rep1.f60287d5ff9f8e8003f3a3b03aec1433.mugqic.done
)
wiggle_20_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_21_JOB_ID: bed_graph.8-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.8-cell_rep2
JOB_DEPENDENCIES=$picard_mark_duplicates_11_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.8-cell_rep2.d589b92d778d3dd8e4849cc5409fb5de.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.8-cell_rep2.d589b92d778d3dd8e4849cc5409fb5de.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/8-cell_rep2  && \
nmblines=$(samtools view -F 256 alignment/8-cell_rep2/8-cell_rep2.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/8-cell_rep2/8-cell_rep2.sorted.mdup.bam \
  -g /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  > tracks/8-cell_rep2/8-cell_rep2.bedGraph
bed_graph.8-cell_rep2.d589b92d778d3dd8e4849cc5409fb5de.mugqic.done
)
wiggle_21_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_21_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_22_JOB_ID: wiggle.8-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.8-cell_rep2
JOB_DEPENDENCIES=$wiggle_21_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.8-cell_rep2.077c574833b2df412a2b3fa5c4150e64.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.8-cell_rep2.077c574833b2df412a2b3fa5c4150e64.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/8-cell_rep2/8-cell_rep2.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/8-cell_rep2/8-cell_rep2.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/8-cell_rep2/8-cell_rep2.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  tracks/bigWig/8-cell_rep2.bw
wiggle.8-cell_rep2.077c574833b2df412a2b3fa5c4150e64.mugqic.done
)
wiggle_22_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_22_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_23_JOB_ID: bed_graph.8-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.8-cell_rep3
JOB_DEPENDENCIES=$picard_mark_duplicates_12_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.8-cell_rep3.bda56d2434831990713c5c241348c4b7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.8-cell_rep3.bda56d2434831990713c5c241348c4b7.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/8-cell_rep3  && \
nmblines=$(samtools view -F 256 alignment/8-cell_rep3/8-cell_rep3.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/8-cell_rep3/8-cell_rep3.sorted.mdup.bam \
  -g /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  > tracks/8-cell_rep3/8-cell_rep3.bedGraph
bed_graph.8-cell_rep3.bda56d2434831990713c5c241348c4b7.mugqic.done
)
wiggle_23_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_23_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_24_JOB_ID: wiggle.8-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.8-cell_rep3
JOB_DEPENDENCIES=$wiggle_23_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.8-cell_rep3.bcf12ec25d9cbdbc48ba0b6c763cf2b8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.8-cell_rep3.bcf12ec25d9cbdbc48ba0b6c763cf2b8.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/8-cell_rep3/8-cell_rep3.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/8-cell_rep3/8-cell_rep3.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/8-cell_rep3/8-cell_rep3.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  tracks/bigWig/8-cell_rep3.bw
wiggle.8-cell_rep3.bcf12ec25d9cbdbc48ba0b6c763cf2b8.mugqic.done
)
wiggle_24_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_24_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_25_JOB_ID: bed_graph.16-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.16-cell_rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_13_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.16-cell_rep1.8eb1e3a3c6ef0877667e7a48ae3ebd3a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.16-cell_rep1.8eb1e3a3c6ef0877667e7a48ae3ebd3a.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/16-cell_rep1  && \
nmblines=$(samtools view -F 256 alignment/16-cell_rep1/16-cell_rep1.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/16-cell_rep1/16-cell_rep1.sorted.mdup.bam \
  -g /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  > tracks/16-cell_rep1/16-cell_rep1.bedGraph
bed_graph.16-cell_rep1.8eb1e3a3c6ef0877667e7a48ae3ebd3a.mugqic.done
)
wiggle_25_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_25_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_26_JOB_ID: wiggle.16-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.16-cell_rep1
JOB_DEPENDENCIES=$wiggle_25_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.16-cell_rep1.5d5c050cc5d839851fa095b4b58126b4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.16-cell_rep1.5d5c050cc5d839851fa095b4b58126b4.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/16-cell_rep1/16-cell_rep1.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/16-cell_rep1/16-cell_rep1.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/16-cell_rep1/16-cell_rep1.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  tracks/bigWig/16-cell_rep1.bw
wiggle.16-cell_rep1.5d5c050cc5d839851fa095b4b58126b4.mugqic.done
)
wiggle_26_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_26_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_27_JOB_ID: bed_graph.16-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.16-cell_rep2
JOB_DEPENDENCIES=$picard_mark_duplicates_14_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.16-cell_rep2.bff382a45b769df0383eff5095ad5753.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.16-cell_rep2.bff382a45b769df0383eff5095ad5753.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/16-cell_rep2  && \
nmblines=$(samtools view -F 256 alignment/16-cell_rep2/16-cell_rep2.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/16-cell_rep2/16-cell_rep2.sorted.mdup.bam \
  -g /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  > tracks/16-cell_rep2/16-cell_rep2.bedGraph
bed_graph.16-cell_rep2.bff382a45b769df0383eff5095ad5753.mugqic.done
)
wiggle_27_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_27_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_28_JOB_ID: wiggle.16-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.16-cell_rep2
JOB_DEPENDENCIES=$wiggle_27_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.16-cell_rep2.2d6a075103f28db7e60dd7ea01c6f912.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.16-cell_rep2.2d6a075103f28db7e60dd7ea01c6f912.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/16-cell_rep2/16-cell_rep2.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/16-cell_rep2/16-cell_rep2.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/16-cell_rep2/16-cell_rep2.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  tracks/bigWig/16-cell_rep2.bw
wiggle.16-cell_rep2.2d6a075103f28db7e60dd7ea01c6f912.mugqic.done
)
wiggle_28_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_28_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_29_JOB_ID: bed_graph.16-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.16-cell_rep3
JOB_DEPENDENCIES=$picard_mark_duplicates_15_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.16-cell_rep3.f3ca5f238aa79c86410d938d835a0153.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.16-cell_rep3.f3ca5f238aa79c86410d938d835a0153.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/16-cell_rep3  && \
nmblines=$(samtools view -F 256 alignment/16-cell_rep3/16-cell_rep3.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/16-cell_rep3/16-cell_rep3.sorted.mdup.bam \
  -g /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  > tracks/16-cell_rep3/16-cell_rep3.bedGraph
bed_graph.16-cell_rep3.f3ca5f238aa79c86410d938d835a0153.mugqic.done
)
wiggle_29_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_29_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_30_JOB_ID: wiggle.16-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.16-cell_rep3
JOB_DEPENDENCIES=$wiggle_29_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.16-cell_rep3.6302961b614b1c2fca2830e59eddcaef.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.16-cell_rep3.6302961b614b1c2fca2830e59eddcaef.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/16-cell_rep3/16-cell_rep3.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/16-cell_rep3/16-cell_rep3.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/16-cell_rep3/16-cell_rep3.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  tracks/bigWig/16-cell_rep3.bw
wiggle.16-cell_rep3.6302961b614b1c2fca2830e59eddcaef.mugqic.done
)
wiggle_30_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_30_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_31_JOB_ID: bed_graph.Blastocyst_rep1
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.Blastocyst_rep1
JOB_DEPENDENCIES=$picard_mark_duplicates_16_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.Blastocyst_rep1.40b8e423deca203bd1b143f8d50f6e71.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.Blastocyst_rep1.40b8e423deca203bd1b143f8d50f6e71.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/Blastocyst_rep1  && \
nmblines=$(samtools view -F 256 alignment/Blastocyst_rep1/Blastocyst_rep1.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/Blastocyst_rep1/Blastocyst_rep1.sorted.mdup.bam \
  -g /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  > tracks/Blastocyst_rep1/Blastocyst_rep1.bedGraph
bed_graph.Blastocyst_rep1.40b8e423deca203bd1b143f8d50f6e71.mugqic.done
)
wiggle_31_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_31_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_32_JOB_ID: wiggle.Blastocyst_rep1
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Blastocyst_rep1
JOB_DEPENDENCIES=$wiggle_31_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.Blastocyst_rep1.1692914fa5cd620191f84d3fd17b633b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Blastocyst_rep1.1692914fa5cd620191f84d3fd17b633b.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Blastocyst_rep1/Blastocyst_rep1.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Blastocyst_rep1/Blastocyst_rep1.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Blastocyst_rep1/Blastocyst_rep1.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  tracks/bigWig/Blastocyst_rep1.bw
wiggle.Blastocyst_rep1.1692914fa5cd620191f84d3fd17b633b.mugqic.done
)
wiggle_32_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_32_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_33_JOB_ID: bed_graph.Blastocyst_rep2
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.Blastocyst_rep2
JOB_DEPENDENCIES=$picard_mark_duplicates_17_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.Blastocyst_rep2.f3dcc6b128c1ba82fb36a408e70ac09c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.Blastocyst_rep2.f3dcc6b128c1ba82fb36a408e70ac09c.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/Blastocyst_rep2  && \
nmblines=$(samtools view -F 256 alignment/Blastocyst_rep2/Blastocyst_rep2.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/Blastocyst_rep2/Blastocyst_rep2.sorted.mdup.bam \
  -g /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  > tracks/Blastocyst_rep2/Blastocyst_rep2.bedGraph
bed_graph.Blastocyst_rep2.f3dcc6b128c1ba82fb36a408e70ac09c.mugqic.done
)
wiggle_33_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_33_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_34_JOB_ID: wiggle.Blastocyst_rep2
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Blastocyst_rep2
JOB_DEPENDENCIES=$wiggle_33_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.Blastocyst_rep2.0cf166c70e5aa240202eb9f3d3e7c738.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Blastocyst_rep2.0cf166c70e5aa240202eb9f3d3e7c738.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Blastocyst_rep2/Blastocyst_rep2.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Blastocyst_rep2/Blastocyst_rep2.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Blastocyst_rep2/Blastocyst_rep2.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  tracks/bigWig/Blastocyst_rep2.bw
wiggle.Blastocyst_rep2.0cf166c70e5aa240202eb9f3d3e7c738.mugqic.done
)
wiggle_34_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_34_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_35_JOB_ID: bed_graph.Blastocyst_rep3
#-------------------------------------------------------------------------------
JOB_NAME=bed_graph.Blastocyst_rep3
JOB_DEPENDENCIES=$picard_mark_duplicates_18_JOB_ID
JOB_DONE=job_output/wiggle/bed_graph.Blastocyst_rep3.9dab56917b687cd6e24cb763ee448230.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bed_graph.Blastocyst_rep3.9dab56917b687cd6e24cb763ee448230.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/bedtools/2.26.0 && \
mkdir -p tracks/Blastocyst_rep3  && \
nmblines=$(samtools view -F 256 alignment/Blastocyst_rep3/Blastocyst_rep3.sorted.mdup.bam | wc -l) && \
scalefactor=0$(echo "scale=2; 1 / ($nmblines / 10000000);" | bc) && \
genomeCoverageBed  -bg -split -scale $scalefactor \
  -ibam alignment/Blastocyst_rep3/Blastocyst_rep3.sorted.mdup.bam \
  -g /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  > tracks/Blastocyst_rep3/Blastocyst_rep3.bedGraph
bed_graph.Blastocyst_rep3.9dab56917b687cd6e24cb763ee448230.mugqic.done
)
wiggle_35_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=38G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_35_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_36_JOB_ID: wiggle.Blastocyst_rep3
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Blastocyst_rep3
JOB_DEPENDENCIES=$wiggle_35_JOB_ID
JOB_DONE=job_output/wiggle/wiggle.Blastocyst_rep3.487d5770258ae5dc3c9d201e3b93967c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Blastocyst_rep3.487d5770258ae5dc3c9d201e3b93967c.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Blastocyst_rep3/Blastocyst_rep3.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Blastocyst_rep3/Blastocyst_rep3.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Blastocyst_rep3/Blastocyst_rep3.bedGraph.sorted \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa.fai \
  tracks/bigWig/Blastocyst_rep3.bw
wiggle.Blastocyst_rep3.487d5770258ae5dc3c9d201e3b93967c.mugqic.done
)
wiggle_36_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_36_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: raw_counts
#-------------------------------------------------------------------------------
STEP=raw_counts
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: raw_counts_1_JOB_ID: htseq_count.GV-oocyte_rep1
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.GV-oocyte_rep1
JOB_DEPENDENCIES=$picard_sort_sam_1_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.GV-oocyte_rep1.70a3fd30d37d1b460e42b3ae90f8359c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.GV-oocyte_rep1.70a3fd30d37d1b460e42b3ae90f8359c.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/GV-oocyte_rep1/GV-oocyte_rep1.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  > raw_counts/GV-oocyte_rep1.readcounts.csv
htseq_count.GV-oocyte_rep1.70a3fd30d37d1b460e42b3ae90f8359c.mugqic.done
)
raw_counts_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_2_JOB_ID: htseq_count.GV-oocyte_rep2
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.GV-oocyte_rep2
JOB_DEPENDENCIES=$picard_sort_sam_2_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.GV-oocyte_rep2.da641897e3a4596a52ce50abdfd6de2b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.GV-oocyte_rep2.da641897e3a4596a52ce50abdfd6de2b.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/GV-oocyte_rep2/GV-oocyte_rep2.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  > raw_counts/GV-oocyte_rep2.readcounts.csv
htseq_count.GV-oocyte_rep2.da641897e3a4596a52ce50abdfd6de2b.mugqic.done
)
raw_counts_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_3_JOB_ID: htseq_count.GV-oocyte_rep3
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.GV-oocyte_rep3
JOB_DEPENDENCIES=$picard_sort_sam_3_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.GV-oocyte_rep3.fd9f53f1ff38a23871d9af2c385c27bd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.GV-oocyte_rep3.fd9f53f1ff38a23871d9af2c385c27bd.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/GV-oocyte_rep3/GV-oocyte_rep3.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  > raw_counts/GV-oocyte_rep3.readcounts.csv
htseq_count.GV-oocyte_rep3.fd9f53f1ff38a23871d9af2c385c27bd.mugqic.done
)
raw_counts_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_4_JOB_ID: htseq_count.MII-oocyte_rep1
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.MII-oocyte_rep1
JOB_DEPENDENCIES=$picard_sort_sam_4_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.MII-oocyte_rep1.60898388e9b4977a630399be15d14fd2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.MII-oocyte_rep1.60898388e9b4977a630399be15d14fd2.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/MII-oocyte_rep1/MII-oocyte_rep1.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  > raw_counts/MII-oocyte_rep1.readcounts.csv
htseq_count.MII-oocyte_rep1.60898388e9b4977a630399be15d14fd2.mugqic.done
)
raw_counts_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_5_JOB_ID: htseq_count.MII-oocyte_rep2
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.MII-oocyte_rep2
JOB_DEPENDENCIES=$picard_sort_sam_5_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.MII-oocyte_rep2.dad1a5c596b2100cddd49543bdabfb12.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.MII-oocyte_rep2.dad1a5c596b2100cddd49543bdabfb12.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/MII-oocyte_rep2/MII-oocyte_rep2.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  > raw_counts/MII-oocyte_rep2.readcounts.csv
htseq_count.MII-oocyte_rep2.dad1a5c596b2100cddd49543bdabfb12.mugqic.done
)
raw_counts_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_6_JOB_ID: htseq_count.MII-oocyte_rep3
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.MII-oocyte_rep3
JOB_DEPENDENCIES=$picard_sort_sam_6_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.MII-oocyte_rep3.cbd3cef5d582cfb8b1e166b923805f9f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.MII-oocyte_rep3.cbd3cef5d582cfb8b1e166b923805f9f.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/MII-oocyte_rep3/MII-oocyte_rep3.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  > raw_counts/MII-oocyte_rep3.readcounts.csv
htseq_count.MII-oocyte_rep3.cbd3cef5d582cfb8b1e166b923805f9f.mugqic.done
)
raw_counts_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_7_JOB_ID: htseq_count.4-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.4-cell_rep1
JOB_DEPENDENCIES=$picard_sort_sam_7_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.4-cell_rep1.7b83135f981558ccc89c9a64b4bf3070.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.4-cell_rep1.7b83135f981558ccc89c9a64b4bf3070.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/4-cell_rep1/4-cell_rep1.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  > raw_counts/4-cell_rep1.readcounts.csv
htseq_count.4-cell_rep1.7b83135f981558ccc89c9a64b4bf3070.mugqic.done
)
raw_counts_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_8_JOB_ID: htseq_count.4-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.4-cell_rep2
JOB_DEPENDENCIES=$picard_sort_sam_8_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.4-cell_rep2.6137d20c8581cf5eb394027461e96c6f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.4-cell_rep2.6137d20c8581cf5eb394027461e96c6f.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/4-cell_rep2/4-cell_rep2.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  > raw_counts/4-cell_rep2.readcounts.csv
htseq_count.4-cell_rep2.6137d20c8581cf5eb394027461e96c6f.mugqic.done
)
raw_counts_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_9_JOB_ID: htseq_count.4-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.4-cell_rep3
JOB_DEPENDENCIES=$picard_sort_sam_9_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.4-cell_rep3.1b22594c3da734b69cc388fe31364795.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.4-cell_rep3.1b22594c3da734b69cc388fe31364795.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/4-cell_rep3/4-cell_rep3.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  > raw_counts/4-cell_rep3.readcounts.csv
htseq_count.4-cell_rep3.1b22594c3da734b69cc388fe31364795.mugqic.done
)
raw_counts_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_10_JOB_ID: htseq_count.8-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.8-cell_rep1
JOB_DEPENDENCIES=$picard_sort_sam_10_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.8-cell_rep1.edd5982dc593c3450efdeb40018baa85.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.8-cell_rep1.edd5982dc593c3450efdeb40018baa85.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/8-cell_rep1/8-cell_rep1.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  > raw_counts/8-cell_rep1.readcounts.csv
htseq_count.8-cell_rep1.edd5982dc593c3450efdeb40018baa85.mugqic.done
)
raw_counts_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_11_JOB_ID: htseq_count.8-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.8-cell_rep2
JOB_DEPENDENCIES=$picard_sort_sam_11_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.8-cell_rep2.f29ea2cf41bc2361004c1ed42b8299f5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.8-cell_rep2.f29ea2cf41bc2361004c1ed42b8299f5.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/8-cell_rep2/8-cell_rep2.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  > raw_counts/8-cell_rep2.readcounts.csv
htseq_count.8-cell_rep2.f29ea2cf41bc2361004c1ed42b8299f5.mugqic.done
)
raw_counts_11_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_12_JOB_ID: htseq_count.8-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.8-cell_rep3
JOB_DEPENDENCIES=$picard_sort_sam_12_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.8-cell_rep3.2a5f3d75b299ec40af1a2af595626c09.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.8-cell_rep3.2a5f3d75b299ec40af1a2af595626c09.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/8-cell_rep3/8-cell_rep3.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  > raw_counts/8-cell_rep3.readcounts.csv
htseq_count.8-cell_rep3.2a5f3d75b299ec40af1a2af595626c09.mugqic.done
)
raw_counts_12_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_13_JOB_ID: htseq_count.16-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.16-cell_rep1
JOB_DEPENDENCIES=$picard_sort_sam_13_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.16-cell_rep1.ca38d5dfeb3ddbf07e089c983ba6c299.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.16-cell_rep1.ca38d5dfeb3ddbf07e089c983ba6c299.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/16-cell_rep1/16-cell_rep1.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  > raw_counts/16-cell_rep1.readcounts.csv
htseq_count.16-cell_rep1.ca38d5dfeb3ddbf07e089c983ba6c299.mugqic.done
)
raw_counts_13_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_14_JOB_ID: htseq_count.16-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.16-cell_rep2
JOB_DEPENDENCIES=$picard_sort_sam_14_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.16-cell_rep2.7f11b847cad00ff2ec5b80a1c8969499.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.16-cell_rep2.7f11b847cad00ff2ec5b80a1c8969499.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/16-cell_rep2/16-cell_rep2.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  > raw_counts/16-cell_rep2.readcounts.csv
htseq_count.16-cell_rep2.7f11b847cad00ff2ec5b80a1c8969499.mugqic.done
)
raw_counts_14_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_15_JOB_ID: htseq_count.16-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.16-cell_rep3
JOB_DEPENDENCIES=$picard_sort_sam_15_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.16-cell_rep3.c90561494bbc74f149e47b39a46e4caf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.16-cell_rep3.c90561494bbc74f149e47b39a46e4caf.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/16-cell_rep3/16-cell_rep3.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  > raw_counts/16-cell_rep3.readcounts.csv
htseq_count.16-cell_rep3.c90561494bbc74f149e47b39a46e4caf.mugqic.done
)
raw_counts_15_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_16_JOB_ID: htseq_count.Blastocyst_rep1
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.Blastocyst_rep1
JOB_DEPENDENCIES=$picard_sort_sam_16_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.Blastocyst_rep1.ecbd044c5cc9444a7def36296c951444.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.Blastocyst_rep1.ecbd044c5cc9444a7def36296c951444.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/Blastocyst_rep1/Blastocyst_rep1.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  > raw_counts/Blastocyst_rep1.readcounts.csv
htseq_count.Blastocyst_rep1.ecbd044c5cc9444a7def36296c951444.mugqic.done
)
raw_counts_16_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_17_JOB_ID: htseq_count.Blastocyst_rep2
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.Blastocyst_rep2
JOB_DEPENDENCIES=$picard_sort_sam_17_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.Blastocyst_rep2.bc06c8a3c0bce09a5d0b4dec4375c88e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.Blastocyst_rep2.bc06c8a3c0bce09a5d0b4dec4375c88e.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/Blastocyst_rep2/Blastocyst_rep2.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  > raw_counts/Blastocyst_rep2.readcounts.csv
htseq_count.Blastocyst_rep2.bc06c8a3c0bce09a5d0b4dec4375c88e.mugqic.done
)
raw_counts_17_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_18_JOB_ID: htseq_count.Blastocyst_rep3
#-------------------------------------------------------------------------------
JOB_NAME=htseq_count.Blastocyst_rep3
JOB_DEPENDENCIES=$picard_sort_sam_18_JOB_ID
JOB_DONE=job_output/raw_counts/htseq_count.Blastocyst_rep3.0a0616c96172f8d60da92fd7ac204a04.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'htseq_count.Blastocyst_rep3.0a0616c96172f8d60da92fd7ac204a04.mugqic.done'
module load mugqic/samtools/1.4.1 mugqic/python/2.7.13 && \
mkdir -p raw_counts && \
samtools view -F 4 \
  alignment/Blastocyst_rep3/Blastocyst_rep3.QueryNameSorted.bam | \
htseq-count - \
  -m intersection-nonempty \
  --stranded=reverse \
  --format=sam \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  > raw_counts/Blastocyst_rep3.readcounts.csv
htseq_count.Blastocyst_rep3.0a0616c96172f8d60da92fd7ac204a04.mugqic.done
)
raw_counts_18_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=24G -N 1 -n 6 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: raw_counts_metrics
#-------------------------------------------------------------------------------
STEP=raw_counts_metrics
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: raw_counts_metrics_1_JOB_ID: metrics.matrix
#-------------------------------------------------------------------------------
JOB_NAME=metrics.matrix
JOB_DEPENDENCIES=$raw_counts_1_JOB_ID:$raw_counts_2_JOB_ID:$raw_counts_3_JOB_ID:$raw_counts_4_JOB_ID:$raw_counts_5_JOB_ID:$raw_counts_6_JOB_ID:$raw_counts_7_JOB_ID:$raw_counts_8_JOB_ID:$raw_counts_9_JOB_ID:$raw_counts_10_JOB_ID:$raw_counts_11_JOB_ID:$raw_counts_12_JOB_ID:$raw_counts_13_JOB_ID:$raw_counts_14_JOB_ID:$raw_counts_15_JOB_ID:$raw_counts_16_JOB_ID:$raw_counts_17_JOB_ID:$raw_counts_18_JOB_ID
JOB_DONE=job_output/raw_counts_metrics/metrics.matrix.edac9787b22aee99930151593e1cb147.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'metrics.matrix.edac9787b22aee99930151593e1cb147.mugqic.done'
module load mugqic/mugqic_tools/2.1.9 && \
mkdir -p DGE && \
gtf2tmpMatrix.awk \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  DGE/tmpMatrix.txt && \
HEAD='Gene\tSymbol' && \
for read_count_file in \
  raw_counts/GV-oocyte_rep1.readcounts.csv \
  raw_counts/GV-oocyte_rep2.readcounts.csv \
  raw_counts/GV-oocyte_rep3.readcounts.csv \
  raw_counts/MII-oocyte_rep1.readcounts.csv \
  raw_counts/MII-oocyte_rep2.readcounts.csv \
  raw_counts/MII-oocyte_rep3.readcounts.csv \
  raw_counts/4-cell_rep1.readcounts.csv \
  raw_counts/4-cell_rep2.readcounts.csv \
  raw_counts/4-cell_rep3.readcounts.csv \
  raw_counts/8-cell_rep1.readcounts.csv \
  raw_counts/8-cell_rep2.readcounts.csv \
  raw_counts/8-cell_rep3.readcounts.csv \
  raw_counts/16-cell_rep1.readcounts.csv \
  raw_counts/16-cell_rep2.readcounts.csv \
  raw_counts/16-cell_rep3.readcounts.csv \
  raw_counts/Blastocyst_rep1.readcounts.csv \
  raw_counts/Blastocyst_rep2.readcounts.csv \
  raw_counts/Blastocyst_rep3.readcounts.csv
do
  sort -k1,1 $read_count_file > DGE/tmpSort.txt && \
  join -1 1 -2 1 <(sort -k1,1 DGE/tmpMatrix.txt) DGE/tmpSort.txt > DGE/tmpMatrix.2.txt && \
  mv DGE/tmpMatrix.2.txt DGE/tmpMatrix.txt && \
  na=$(basename $read_count_file | rev | cut -d. -f3- | rev) && \
  HEAD="$HEAD\t$na"
done && \
echo -e $HEAD | cat - DGE/tmpMatrix.txt | tr ' ' '\t' > DGE/rawCountMatrix.csv && \
rm DGE/tmpSort.txt DGE/tmpMatrix.txt
metrics.matrix.edac9787b22aee99930151593e1cb147.mugqic.done
)
raw_counts_metrics_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=5:00:0 --mem=4G -N 1 -n 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_metrics_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_metrics_2_JOB_ID: metrics.wigzip
#-------------------------------------------------------------------------------
JOB_NAME=metrics.wigzip
JOB_DEPENDENCIES=$wiggle_2_JOB_ID:$wiggle_4_JOB_ID:$wiggle_6_JOB_ID:$wiggle_8_JOB_ID:$wiggle_10_JOB_ID:$wiggle_12_JOB_ID:$wiggle_14_JOB_ID:$wiggle_16_JOB_ID:$wiggle_18_JOB_ID:$wiggle_20_JOB_ID:$wiggle_22_JOB_ID:$wiggle_24_JOB_ID:$wiggle_26_JOB_ID:$wiggle_28_JOB_ID:$wiggle_30_JOB_ID:$wiggle_32_JOB_ID:$wiggle_34_JOB_ID:$wiggle_36_JOB_ID
JOB_DONE=job_output/raw_counts_metrics/metrics.wigzip.edc4e268c60b94d072db60163e113f9c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'metrics.wigzip.edc4e268c60b94d072db60163e113f9c.mugqic.done'
zip -r tracks.zip tracks/bigWig
metrics.wigzip.edc4e268c60b94d072db60163e113f9c.mugqic.done
)
raw_counts_metrics_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=5:00:0 --mem=4G -N 1 -n 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_metrics_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_metrics_3_JOB_ID: rpkm_saturation
#-------------------------------------------------------------------------------
JOB_NAME=rpkm_saturation
JOB_DEPENDENCIES=$raw_counts_metrics_1_JOB_ID
JOB_DONE=job_output/raw_counts_metrics/rpkm_saturation.c7bb1b86a74e0d3b1707d732721d3806.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'rpkm_saturation.c7bb1b86a74e0d3b1707d732721d3806.mugqic.done'
module load mugqic/R_Bioconductor/3.4.2_3.6 mugqic/mugqic_tools/2.1.9 && \
mkdir -p metrics/saturation && \
Rscript $R_TOOLS/rpkmSaturation.R \
  DGE/rawCountMatrix.csv \
  /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.genes.length.tsv \
  raw_counts \
  metrics/saturation \
  15 \
  1 && \
zip -r metrics/saturation.zip metrics/saturation
rpkm_saturation.c7bb1b86a74e0d3b1707d732721d3806.mugqic.done
)
raw_counts_metrics_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"raw_counts_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=128G -N 1 -n 16 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_metrics_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_metrics_4_JOB_ID: raw_count_metrics_report
#-------------------------------------------------------------------------------
JOB_NAME=raw_count_metrics_report
JOB_DEPENDENCIES=$rnaseqc_1_JOB_ID:$raw_counts_metrics_2_JOB_ID:$raw_counts_metrics_3_JOB_ID
JOB_DONE=job_output/raw_counts_metrics/raw_count_metrics_report.e4faf9a0ac8b5cf62e4257c28ff6a716.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'raw_count_metrics_report.e4faf9a0ac8b5cf62e4257c28ff6a716.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
cp metrics/rnaseqRep/corrMatrixSpearman.txt report/corrMatrixSpearman.tsv && \
cp tracks.zip report/ && \
cp metrics/saturation.zip report/ && \
pandoc --to=markdown \
  --template /home/efournie/genpipes/bfx/report/RnaSeq.raw_counts_metrics.md \
  --variable corr_matrix_spearman_table="`head -16 report/corrMatrixSpearman.tsv | cut -f-16| awk -F"	" '{OFS="	"; if (NR==1) {$0="Vs"$0; print; gsub(/[^	]/, "-"); print} else {printf $1; for (i=2; i<=NF; i++) {printf "	"sprintf("%.2f", $i)}; print ""}}' | sed 's/	/|/g'`" \
  /home/efournie/genpipes/bfx/report/RnaSeq.raw_counts_metrics.md \
  > report/RnaSeq.raw_counts_metrics.md
raw_count_metrics_report.e4faf9a0ac8b5cf62e4257c28ff6a716.mugqic.done
)
raw_counts_metrics_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_metrics_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: cufflinks
#-------------------------------------------------------------------------------
STEP=cufflinks
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: cufflinks_1_JOB_ID: cufflinks.GV-oocyte_rep1
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.GV-oocyte_rep1
JOB_DEPENDENCIES=$bam_hard_clip_1_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.GV-oocyte_rep1.f019433152fcbb5eaf99b7557b631ed1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.GV-oocyte_rep1.f019433152fcbb5eaf99b7557b631ed1.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/GV-oocyte_rep1 && \
cufflinks -q  \
  --GTF-guide /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/GV-oocyte_rep1 \
  --num-threads 8 \
  alignment/GV-oocyte_rep1/GV-oocyte_rep1.sorted.mdup.hardClip.bam
cufflinks.GV-oocyte_rep1.f019433152fcbb5eaf99b7557b631ed1.mugqic.done
)
cufflinks_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cufflinks_2_JOB_ID: cufflinks.GV-oocyte_rep2
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.GV-oocyte_rep2
JOB_DEPENDENCIES=$bam_hard_clip_2_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.GV-oocyte_rep2.ce9a9cc644274df53163461982bbdab8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.GV-oocyte_rep2.ce9a9cc644274df53163461982bbdab8.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/GV-oocyte_rep2 && \
cufflinks -q  \
  --GTF-guide /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/GV-oocyte_rep2 \
  --num-threads 8 \
  alignment/GV-oocyte_rep2/GV-oocyte_rep2.sorted.mdup.hardClip.bam
cufflinks.GV-oocyte_rep2.ce9a9cc644274df53163461982bbdab8.mugqic.done
)
cufflinks_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cufflinks_3_JOB_ID: cufflinks.GV-oocyte_rep3
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.GV-oocyte_rep3
JOB_DEPENDENCIES=$bam_hard_clip_3_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.GV-oocyte_rep3.18047e30184d6b897e4d9bbe4ecca0d2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.GV-oocyte_rep3.18047e30184d6b897e4d9bbe4ecca0d2.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/GV-oocyte_rep3 && \
cufflinks -q  \
  --GTF-guide /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/GV-oocyte_rep3 \
  --num-threads 8 \
  alignment/GV-oocyte_rep3/GV-oocyte_rep3.sorted.mdup.hardClip.bam
cufflinks.GV-oocyte_rep3.18047e30184d6b897e4d9bbe4ecca0d2.mugqic.done
)
cufflinks_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cufflinks_4_JOB_ID: cufflinks.MII-oocyte_rep1
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.MII-oocyte_rep1
JOB_DEPENDENCIES=$bam_hard_clip_4_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.MII-oocyte_rep1.f4c86721be2d09a9d9e431e84faa0c5d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.MII-oocyte_rep1.f4c86721be2d09a9d9e431e84faa0c5d.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/MII-oocyte_rep1 && \
cufflinks -q  \
  --GTF-guide /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/MII-oocyte_rep1 \
  --num-threads 8 \
  alignment/MII-oocyte_rep1/MII-oocyte_rep1.sorted.mdup.hardClip.bam
cufflinks.MII-oocyte_rep1.f4c86721be2d09a9d9e431e84faa0c5d.mugqic.done
)
cufflinks_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cufflinks_5_JOB_ID: cufflinks.MII-oocyte_rep2
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.MII-oocyte_rep2
JOB_DEPENDENCIES=$bam_hard_clip_5_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.MII-oocyte_rep2.6027f9152ddfa2123a52cb5849685419.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.MII-oocyte_rep2.6027f9152ddfa2123a52cb5849685419.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/MII-oocyte_rep2 && \
cufflinks -q  \
  --GTF-guide /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/MII-oocyte_rep2 \
  --num-threads 8 \
  alignment/MII-oocyte_rep2/MII-oocyte_rep2.sorted.mdup.hardClip.bam
cufflinks.MII-oocyte_rep2.6027f9152ddfa2123a52cb5849685419.mugqic.done
)
cufflinks_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cufflinks_6_JOB_ID: cufflinks.MII-oocyte_rep3
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.MII-oocyte_rep3
JOB_DEPENDENCIES=$bam_hard_clip_6_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.MII-oocyte_rep3.a1633f3957966e8683e8deed1f1a55d1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.MII-oocyte_rep3.a1633f3957966e8683e8deed1f1a55d1.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/MII-oocyte_rep3 && \
cufflinks -q  \
  --GTF-guide /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/MII-oocyte_rep3 \
  --num-threads 8 \
  alignment/MII-oocyte_rep3/MII-oocyte_rep3.sorted.mdup.hardClip.bam
cufflinks.MII-oocyte_rep3.a1633f3957966e8683e8deed1f1a55d1.mugqic.done
)
cufflinks_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cufflinks_7_JOB_ID: cufflinks.4-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.4-cell_rep1
JOB_DEPENDENCIES=$bam_hard_clip_7_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.4-cell_rep1.8ea8f52497ac75ae5dbb25fcbbe2bdce.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.4-cell_rep1.8ea8f52497ac75ae5dbb25fcbbe2bdce.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/4-cell_rep1 && \
cufflinks -q  \
  --GTF-guide /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/4-cell_rep1 \
  --num-threads 8 \
  alignment/4-cell_rep1/4-cell_rep1.sorted.mdup.hardClip.bam
cufflinks.4-cell_rep1.8ea8f52497ac75ae5dbb25fcbbe2bdce.mugqic.done
)
cufflinks_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cufflinks_8_JOB_ID: cufflinks.4-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.4-cell_rep2
JOB_DEPENDENCIES=$bam_hard_clip_8_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.4-cell_rep2.76adf95d1e7af4fe00098a405d5762fa.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.4-cell_rep2.76adf95d1e7af4fe00098a405d5762fa.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/4-cell_rep2 && \
cufflinks -q  \
  --GTF-guide /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/4-cell_rep2 \
  --num-threads 8 \
  alignment/4-cell_rep2/4-cell_rep2.sorted.mdup.hardClip.bam
cufflinks.4-cell_rep2.76adf95d1e7af4fe00098a405d5762fa.mugqic.done
)
cufflinks_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cufflinks_9_JOB_ID: cufflinks.4-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.4-cell_rep3
JOB_DEPENDENCIES=$bam_hard_clip_9_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.4-cell_rep3.0f7450049fd997174f06570d4bcea682.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.4-cell_rep3.0f7450049fd997174f06570d4bcea682.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/4-cell_rep3 && \
cufflinks -q  \
  --GTF-guide /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/4-cell_rep3 \
  --num-threads 8 \
  alignment/4-cell_rep3/4-cell_rep3.sorted.mdup.hardClip.bam
cufflinks.4-cell_rep3.0f7450049fd997174f06570d4bcea682.mugqic.done
)
cufflinks_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cufflinks_10_JOB_ID: cufflinks.8-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.8-cell_rep1
JOB_DEPENDENCIES=$bam_hard_clip_10_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.8-cell_rep1.650d97d98fd93a90ffd39409104a4cd8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.8-cell_rep1.650d97d98fd93a90ffd39409104a4cd8.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/8-cell_rep1 && \
cufflinks -q  \
  --GTF-guide /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/8-cell_rep1 \
  --num-threads 8 \
  alignment/8-cell_rep1/8-cell_rep1.sorted.mdup.hardClip.bam
cufflinks.8-cell_rep1.650d97d98fd93a90ffd39409104a4cd8.mugqic.done
)
cufflinks_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cufflinks_11_JOB_ID: cufflinks.8-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.8-cell_rep2
JOB_DEPENDENCIES=$bam_hard_clip_11_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.8-cell_rep2.f1c170334b47a650a5ec89c9565c1984.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.8-cell_rep2.f1c170334b47a650a5ec89c9565c1984.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/8-cell_rep2 && \
cufflinks -q  \
  --GTF-guide /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/8-cell_rep2 \
  --num-threads 8 \
  alignment/8-cell_rep2/8-cell_rep2.sorted.mdup.hardClip.bam
cufflinks.8-cell_rep2.f1c170334b47a650a5ec89c9565c1984.mugqic.done
)
cufflinks_11_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cufflinks_12_JOB_ID: cufflinks.8-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.8-cell_rep3
JOB_DEPENDENCIES=$bam_hard_clip_12_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.8-cell_rep3.a09bc0e90efc813dd3c849f3dd5f4604.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.8-cell_rep3.a09bc0e90efc813dd3c849f3dd5f4604.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/8-cell_rep3 && \
cufflinks -q  \
  --GTF-guide /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/8-cell_rep3 \
  --num-threads 8 \
  alignment/8-cell_rep3/8-cell_rep3.sorted.mdup.hardClip.bam
cufflinks.8-cell_rep3.a09bc0e90efc813dd3c849f3dd5f4604.mugqic.done
)
cufflinks_12_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cufflinks_13_JOB_ID: cufflinks.16-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.16-cell_rep1
JOB_DEPENDENCIES=$bam_hard_clip_13_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.16-cell_rep1.6aca63a9cd748c2d268450aa30222500.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.16-cell_rep1.6aca63a9cd748c2d268450aa30222500.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/16-cell_rep1 && \
cufflinks -q  \
  --GTF-guide /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/16-cell_rep1 \
  --num-threads 8 \
  alignment/16-cell_rep1/16-cell_rep1.sorted.mdup.hardClip.bam
cufflinks.16-cell_rep1.6aca63a9cd748c2d268450aa30222500.mugqic.done
)
cufflinks_13_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cufflinks_14_JOB_ID: cufflinks.16-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.16-cell_rep2
JOB_DEPENDENCIES=$bam_hard_clip_14_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.16-cell_rep2.978e14a739fed64b914ac07c5468f2a8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.16-cell_rep2.978e14a739fed64b914ac07c5468f2a8.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/16-cell_rep2 && \
cufflinks -q  \
  --GTF-guide /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/16-cell_rep2 \
  --num-threads 8 \
  alignment/16-cell_rep2/16-cell_rep2.sorted.mdup.hardClip.bam
cufflinks.16-cell_rep2.978e14a739fed64b914ac07c5468f2a8.mugqic.done
)
cufflinks_14_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cufflinks_15_JOB_ID: cufflinks.16-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.16-cell_rep3
JOB_DEPENDENCIES=$bam_hard_clip_15_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.16-cell_rep3.2825054f3f22d1576037c662d6741473.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.16-cell_rep3.2825054f3f22d1576037c662d6741473.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/16-cell_rep3 && \
cufflinks -q  \
  --GTF-guide /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/16-cell_rep3 \
  --num-threads 8 \
  alignment/16-cell_rep3/16-cell_rep3.sorted.mdup.hardClip.bam
cufflinks.16-cell_rep3.2825054f3f22d1576037c662d6741473.mugqic.done
)
cufflinks_15_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cufflinks_16_JOB_ID: cufflinks.Blastocyst_rep1
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.Blastocyst_rep1
JOB_DEPENDENCIES=$bam_hard_clip_16_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.Blastocyst_rep1.f041796eb8c71524df94492c0402e860.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.Blastocyst_rep1.f041796eb8c71524df94492c0402e860.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/Blastocyst_rep1 && \
cufflinks -q  \
  --GTF-guide /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/Blastocyst_rep1 \
  --num-threads 8 \
  alignment/Blastocyst_rep1/Blastocyst_rep1.sorted.mdup.hardClip.bam
cufflinks.Blastocyst_rep1.f041796eb8c71524df94492c0402e860.mugqic.done
)
cufflinks_16_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cufflinks_17_JOB_ID: cufflinks.Blastocyst_rep2
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.Blastocyst_rep2
JOB_DEPENDENCIES=$bam_hard_clip_17_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.Blastocyst_rep2.d4511dea32498f26de5dc5e997576657.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.Blastocyst_rep2.d4511dea32498f26de5dc5e997576657.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/Blastocyst_rep2 && \
cufflinks -q  \
  --GTF-guide /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/Blastocyst_rep2 \
  --num-threads 8 \
  alignment/Blastocyst_rep2/Blastocyst_rep2.sorted.mdup.hardClip.bam
cufflinks.Blastocyst_rep2.d4511dea32498f26de5dc5e997576657.mugqic.done
)
cufflinks_17_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cufflinks_18_JOB_ID: cufflinks.Blastocyst_rep3
#-------------------------------------------------------------------------------
JOB_NAME=cufflinks.Blastocyst_rep3
JOB_DEPENDENCIES=$bam_hard_clip_18_JOB_ID
JOB_DONE=job_output/cufflinks/cufflinks.Blastocyst_rep3.9ab48835bd2927fbe9eca11dc085b518.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cufflinks.Blastocyst_rep3.9ab48835bd2927fbe9eca11dc085b518.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/Blastocyst_rep3 && \
cufflinks -q  \
  --GTF-guide /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/Blastocyst_rep3 \
  --num-threads 8 \
  alignment/Blastocyst_rep3/Blastocyst_rep3.sorted.mdup.hardClip.bam
cufflinks.Blastocyst_rep3.9ab48835bd2927fbe9eca11dc085b518.mugqic.done
)
cufflinks_18_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cufflinks\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cufflinks_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: cuffmerge
#-------------------------------------------------------------------------------
STEP=cuffmerge
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: cuffmerge_1_JOB_ID: cuffmerge
#-------------------------------------------------------------------------------
JOB_NAME=cuffmerge
JOB_DEPENDENCIES=$cufflinks_1_JOB_ID:$cufflinks_2_JOB_ID:$cufflinks_3_JOB_ID:$cufflinks_4_JOB_ID:$cufflinks_5_JOB_ID:$cufflinks_6_JOB_ID:$cufflinks_7_JOB_ID:$cufflinks_8_JOB_ID:$cufflinks_9_JOB_ID:$cufflinks_10_JOB_ID:$cufflinks_11_JOB_ID:$cufflinks_12_JOB_ID:$cufflinks_13_JOB_ID:$cufflinks_14_JOB_ID:$cufflinks_15_JOB_ID:$cufflinks_16_JOB_ID:$cufflinks_17_JOB_ID:$cufflinks_18_JOB_ID
JOB_DONE=job_output/cuffmerge/cuffmerge.397e5b9a47b9440fc517b96a07fcb984.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffmerge.397e5b9a47b9440fc517b96a07fcb984.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/AllSamples && \
`cat > cufflinks/cuffmerge.samples.txt << END
cufflinks/GV-oocyte_rep1/transcripts.gtf
cufflinks/GV-oocyte_rep2/transcripts.gtf
cufflinks/GV-oocyte_rep3/transcripts.gtf
cufflinks/MII-oocyte_rep1/transcripts.gtf
cufflinks/MII-oocyte_rep2/transcripts.gtf
cufflinks/MII-oocyte_rep3/transcripts.gtf
cufflinks/4-cell_rep1/transcripts.gtf
cufflinks/4-cell_rep2/transcripts.gtf
cufflinks/4-cell_rep3/transcripts.gtf
cufflinks/8-cell_rep1/transcripts.gtf
cufflinks/8-cell_rep2/transcripts.gtf
cufflinks/8-cell_rep3/transcripts.gtf
cufflinks/16-cell_rep1/transcripts.gtf
cufflinks/16-cell_rep2/transcripts.gtf
cufflinks/16-cell_rep3/transcripts.gtf
cufflinks/Blastocyst_rep1/transcripts.gtf
cufflinks/Blastocyst_rep2/transcripts.gtf
cufflinks/Blastocyst_rep3/transcripts.gtf
END

` && \
cuffmerge  \
  --ref-gtf /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.gtf \
  --ref-sequence /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
  -o cufflinks/AllSamples \
  --num-threads 8 \
  cufflinks/cuffmerge.samples.txt
cuffmerge.397e5b9a47b9440fc517b96a07fcb984.mugqic.done
)
cuffmerge_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffmerge\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffmerge\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffmerge_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: cuffquant
#-------------------------------------------------------------------------------
STEP=cuffquant
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: cuffquant_1_JOB_ID: cuffquant.GV-oocyte_rep1
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.GV-oocyte_rep1
JOB_DEPENDENCIES=$bam_hard_clip_1_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.GV-oocyte_rep1.28d0c1ec7bd132353ba82169df0cd9f4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.GV-oocyte_rep1.28d0c1ec7bd132353ba82169df0cd9f4.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/GV-oocyte_rep1 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/GV-oocyte_rep1 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/GV-oocyte_rep1/GV-oocyte_rep1.sorted.mdup.hardClip.bam
cuffquant.GV-oocyte_rep1.28d0c1ec7bd132353ba82169df0cd9f4.mugqic.done
)
cuffquant_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffquant_2_JOB_ID: cuffquant.GV-oocyte_rep2
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.GV-oocyte_rep2
JOB_DEPENDENCIES=$bam_hard_clip_2_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.GV-oocyte_rep2.36c2baaf85f6f843a9b8f88ffa4e8105.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.GV-oocyte_rep2.36c2baaf85f6f843a9b8f88ffa4e8105.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/GV-oocyte_rep2 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/GV-oocyte_rep2 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/GV-oocyte_rep2/GV-oocyte_rep2.sorted.mdup.hardClip.bam
cuffquant.GV-oocyte_rep2.36c2baaf85f6f843a9b8f88ffa4e8105.mugqic.done
)
cuffquant_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffquant_3_JOB_ID: cuffquant.GV-oocyte_rep3
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.GV-oocyte_rep3
JOB_DEPENDENCIES=$bam_hard_clip_3_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.GV-oocyte_rep3.789ff788a6fd1af79156e7199687eb77.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.GV-oocyte_rep3.789ff788a6fd1af79156e7199687eb77.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/GV-oocyte_rep3 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/GV-oocyte_rep3 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/GV-oocyte_rep3/GV-oocyte_rep3.sorted.mdup.hardClip.bam
cuffquant.GV-oocyte_rep3.789ff788a6fd1af79156e7199687eb77.mugqic.done
)
cuffquant_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffquant_4_JOB_ID: cuffquant.MII-oocyte_rep1
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.MII-oocyte_rep1
JOB_DEPENDENCIES=$bam_hard_clip_4_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.MII-oocyte_rep1.1fd769508e522853fb56e142c3b797fe.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.MII-oocyte_rep1.1fd769508e522853fb56e142c3b797fe.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/MII-oocyte_rep1 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/MII-oocyte_rep1 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/MII-oocyte_rep1/MII-oocyte_rep1.sorted.mdup.hardClip.bam
cuffquant.MII-oocyte_rep1.1fd769508e522853fb56e142c3b797fe.mugqic.done
)
cuffquant_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffquant_5_JOB_ID: cuffquant.MII-oocyte_rep2
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.MII-oocyte_rep2
JOB_DEPENDENCIES=$bam_hard_clip_5_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.MII-oocyte_rep2.8dbc44c26ffb0e5b2b35d5b468cb0bda.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.MII-oocyte_rep2.8dbc44c26ffb0e5b2b35d5b468cb0bda.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/MII-oocyte_rep2 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/MII-oocyte_rep2 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/MII-oocyte_rep2/MII-oocyte_rep2.sorted.mdup.hardClip.bam
cuffquant.MII-oocyte_rep2.8dbc44c26ffb0e5b2b35d5b468cb0bda.mugqic.done
)
cuffquant_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffquant_6_JOB_ID: cuffquant.MII-oocyte_rep3
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.MII-oocyte_rep3
JOB_DEPENDENCIES=$bam_hard_clip_6_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.MII-oocyte_rep3.de831c624fd1f092cebe7f6add366752.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.MII-oocyte_rep3.de831c624fd1f092cebe7f6add366752.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/MII-oocyte_rep3 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/MII-oocyte_rep3 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/MII-oocyte_rep3/MII-oocyte_rep3.sorted.mdup.hardClip.bam
cuffquant.MII-oocyte_rep3.de831c624fd1f092cebe7f6add366752.mugqic.done
)
cuffquant_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffquant_7_JOB_ID: cuffquant.4-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.4-cell_rep1
JOB_DEPENDENCIES=$bam_hard_clip_7_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.4-cell_rep1.e08efc8f0a8a4f5ecd742ad65b2b2901.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.4-cell_rep1.e08efc8f0a8a4f5ecd742ad65b2b2901.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/4-cell_rep1 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/4-cell_rep1 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/4-cell_rep1/4-cell_rep1.sorted.mdup.hardClip.bam
cuffquant.4-cell_rep1.e08efc8f0a8a4f5ecd742ad65b2b2901.mugqic.done
)
cuffquant_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffquant_8_JOB_ID: cuffquant.4-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.4-cell_rep2
JOB_DEPENDENCIES=$bam_hard_clip_8_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.4-cell_rep2.88a9981c81794699b5425bc6e5b43715.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.4-cell_rep2.88a9981c81794699b5425bc6e5b43715.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/4-cell_rep2 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/4-cell_rep2 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/4-cell_rep2/4-cell_rep2.sorted.mdup.hardClip.bam
cuffquant.4-cell_rep2.88a9981c81794699b5425bc6e5b43715.mugqic.done
)
cuffquant_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffquant_9_JOB_ID: cuffquant.4-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.4-cell_rep3
JOB_DEPENDENCIES=$bam_hard_clip_9_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.4-cell_rep3.cec97b632fc1bd8543d4bfd1e45fa0e8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.4-cell_rep3.cec97b632fc1bd8543d4bfd1e45fa0e8.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/4-cell_rep3 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/4-cell_rep3 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/4-cell_rep3/4-cell_rep3.sorted.mdup.hardClip.bam
cuffquant.4-cell_rep3.cec97b632fc1bd8543d4bfd1e45fa0e8.mugqic.done
)
cuffquant_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffquant_10_JOB_ID: cuffquant.8-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.8-cell_rep1
JOB_DEPENDENCIES=$bam_hard_clip_10_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.8-cell_rep1.a5d6d99f8c20797d0efed4f6f86f2fa8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.8-cell_rep1.a5d6d99f8c20797d0efed4f6f86f2fa8.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/8-cell_rep1 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/8-cell_rep1 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/8-cell_rep1/8-cell_rep1.sorted.mdup.hardClip.bam
cuffquant.8-cell_rep1.a5d6d99f8c20797d0efed4f6f86f2fa8.mugqic.done
)
cuffquant_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffquant_11_JOB_ID: cuffquant.8-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.8-cell_rep2
JOB_DEPENDENCIES=$bam_hard_clip_11_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.8-cell_rep2.d5b61197bdc317a24507c07ce089aaf9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.8-cell_rep2.d5b61197bdc317a24507c07ce089aaf9.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/8-cell_rep2 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/8-cell_rep2 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/8-cell_rep2/8-cell_rep2.sorted.mdup.hardClip.bam
cuffquant.8-cell_rep2.d5b61197bdc317a24507c07ce089aaf9.mugqic.done
)
cuffquant_11_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffquant_12_JOB_ID: cuffquant.8-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.8-cell_rep3
JOB_DEPENDENCIES=$bam_hard_clip_12_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.8-cell_rep3.467552e4bb3bf8101f75c2e60a0fa813.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.8-cell_rep3.467552e4bb3bf8101f75c2e60a0fa813.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/8-cell_rep3 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/8-cell_rep3 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/8-cell_rep3/8-cell_rep3.sorted.mdup.hardClip.bam
cuffquant.8-cell_rep3.467552e4bb3bf8101f75c2e60a0fa813.mugqic.done
)
cuffquant_12_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffquant_13_JOB_ID: cuffquant.16-cell_rep1
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.16-cell_rep1
JOB_DEPENDENCIES=$bam_hard_clip_13_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.16-cell_rep1.9ab83f9a0ee3d00a35ddd72c52834397.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.16-cell_rep1.9ab83f9a0ee3d00a35ddd72c52834397.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/16-cell_rep1 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/16-cell_rep1 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/16-cell_rep1/16-cell_rep1.sorted.mdup.hardClip.bam
cuffquant.16-cell_rep1.9ab83f9a0ee3d00a35ddd72c52834397.mugqic.done
)
cuffquant_13_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffquant_14_JOB_ID: cuffquant.16-cell_rep2
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.16-cell_rep2
JOB_DEPENDENCIES=$bam_hard_clip_14_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.16-cell_rep2.3d0a3f61c5233540e11303b71d71621c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.16-cell_rep2.3d0a3f61c5233540e11303b71d71621c.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/16-cell_rep2 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/16-cell_rep2 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/16-cell_rep2/16-cell_rep2.sorted.mdup.hardClip.bam
cuffquant.16-cell_rep2.3d0a3f61c5233540e11303b71d71621c.mugqic.done
)
cuffquant_14_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffquant_15_JOB_ID: cuffquant.16-cell_rep3
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.16-cell_rep3
JOB_DEPENDENCIES=$bam_hard_clip_15_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.16-cell_rep3.be24cf386319008481489663c0ac3262.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.16-cell_rep3.be24cf386319008481489663c0ac3262.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/16-cell_rep3 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/16-cell_rep3 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/16-cell_rep3/16-cell_rep3.sorted.mdup.hardClip.bam
cuffquant.16-cell_rep3.be24cf386319008481489663c0ac3262.mugqic.done
)
cuffquant_15_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffquant_16_JOB_ID: cuffquant.Blastocyst_rep1
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.Blastocyst_rep1
JOB_DEPENDENCIES=$bam_hard_clip_16_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.Blastocyst_rep1.5a75cdd511dbba3d904e847a2d46f8b7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.Blastocyst_rep1.5a75cdd511dbba3d904e847a2d46f8b7.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/Blastocyst_rep1 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/Blastocyst_rep1 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/Blastocyst_rep1/Blastocyst_rep1.sorted.mdup.hardClip.bam
cuffquant.Blastocyst_rep1.5a75cdd511dbba3d904e847a2d46f8b7.mugqic.done
)
cuffquant_16_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffquant_17_JOB_ID: cuffquant.Blastocyst_rep2
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.Blastocyst_rep2
JOB_DEPENDENCIES=$bam_hard_clip_17_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.Blastocyst_rep2.7e3c25cc9f5be1bd4d872258b15a1fca.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.Blastocyst_rep2.7e3c25cc9f5be1bd4d872258b15a1fca.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/Blastocyst_rep2 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/Blastocyst_rep2 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/Blastocyst_rep2/Blastocyst_rep2.sorted.mdup.hardClip.bam
cuffquant.Blastocyst_rep2.7e3c25cc9f5be1bd4d872258b15a1fca.mugqic.done
)
cuffquant_17_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffquant_18_JOB_ID: cuffquant.Blastocyst_rep3
#-------------------------------------------------------------------------------
JOB_NAME=cuffquant.Blastocyst_rep3
JOB_DEPENDENCIES=$bam_hard_clip_18_JOB_ID:$cuffmerge_1_JOB_ID
JOB_DONE=job_output/cuffquant/cuffquant.Blastocyst_rep3.ba2e18bd82238f99a6358ce40d7f65ea.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffquant.Blastocyst_rep3.ba2e18bd82238f99a6358ce40d7f65ea.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cufflinks/Blastocyst_rep3 && \
cuffquant -q  \
  --max-bundle-frags 1000000 \
  --library-type fr-firststrand \
  --output-dir cufflinks/Blastocyst_rep3 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  alignment/Blastocyst_rep3/Blastocyst_rep3.sorted.mdup.hardClip.bam
cuffquant.Blastocyst_rep3.ba2e18bd82238f99a6358ce40d7f65ea.mugqic.done
)
cuffquant_18_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffquant\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffquant_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: cuffdiff
#-------------------------------------------------------------------------------
STEP=cuffdiff
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: cuffdiff_1_JOB_ID: cuffdiff.MII_vs_GV
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.MII_vs_GV
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID:$cuffquant_1_JOB_ID:$cuffquant_2_JOB_ID:$cuffquant_3_JOB_ID:$cuffquant_4_JOB_ID:$cuffquant_5_JOB_ID:$cuffquant_6_JOB_ID
JOB_DONE=job_output/cuffdiff/cuffdiff.MII_vs_GV.3b467fe8eb2d9010006da5df1e1c0ece.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.MII_vs_GV.3b467fe8eb2d9010006da5df1e1c0ece.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/MII_vs_GV && \
cuffdiff -u \
  --frag-bias-correct /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/MII_vs_GV \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/GV-oocyte_rep1/abundances.cxb,cufflinks/GV-oocyte_rep2/abundances.cxb,cufflinks/GV-oocyte_rep3/abundances.cxb \
  cufflinks/MII-oocyte_rep1/abundances.cxb,cufflinks/MII-oocyte_rep2/abundances.cxb,cufflinks/MII-oocyte_rep3/abundances.cxb
cuffdiff.MII_vs_GV.3b467fe8eb2d9010006da5df1e1c0ece.mugqic.done
)
cuffdiff_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffdiff_2_JOB_ID: cuffdiff.4-cell_vs_MII
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.4-cell_vs_MII
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID:$cuffquant_4_JOB_ID:$cuffquant_5_JOB_ID:$cuffquant_6_JOB_ID:$cuffquant_7_JOB_ID:$cuffquant_8_JOB_ID:$cuffquant_9_JOB_ID
JOB_DONE=job_output/cuffdiff/cuffdiff.4-cell_vs_MII.cf4a0cf5f60bc419e81e987f54cf4436.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.4-cell_vs_MII.cf4a0cf5f60bc419e81e987f54cf4436.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/4-cell_vs_MII && \
cuffdiff -u \
  --frag-bias-correct /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/4-cell_vs_MII \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/MII-oocyte_rep1/abundances.cxb,cufflinks/MII-oocyte_rep2/abundances.cxb,cufflinks/MII-oocyte_rep3/abundances.cxb \
  cufflinks/4-cell_rep1/abundances.cxb,cufflinks/4-cell_rep2/abundances.cxb,cufflinks/4-cell_rep3/abundances.cxb
cuffdiff.4-cell_vs_MII.cf4a0cf5f60bc419e81e987f54cf4436.mugqic.done
)
cuffdiff_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffdiff_3_JOB_ID: cuffdiff.8-cell_vs_4-cell
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.8-cell_vs_4-cell
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID:$cuffquant_7_JOB_ID:$cuffquant_8_JOB_ID:$cuffquant_9_JOB_ID:$cuffquant_10_JOB_ID:$cuffquant_11_JOB_ID:$cuffquant_12_JOB_ID
JOB_DONE=job_output/cuffdiff/cuffdiff.8-cell_vs_4-cell.39a32316c22dd942a0c5e6cc12b8c78a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.8-cell_vs_4-cell.39a32316c22dd942a0c5e6cc12b8c78a.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/8-cell_vs_4-cell && \
cuffdiff -u \
  --frag-bias-correct /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/8-cell_vs_4-cell \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/4-cell_rep1/abundances.cxb,cufflinks/4-cell_rep2/abundances.cxb,cufflinks/4-cell_rep3/abundances.cxb \
  cufflinks/8-cell_rep1/abundances.cxb,cufflinks/8-cell_rep2/abundances.cxb,cufflinks/8-cell_rep3/abundances.cxb
cuffdiff.8-cell_vs_4-cell.39a32316c22dd942a0c5e6cc12b8c78a.mugqic.done
)
cuffdiff_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffdiff_4_JOB_ID: cuffdiff.16-cell_vs_8-cell
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.16-cell_vs_8-cell
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID:$cuffquant_10_JOB_ID:$cuffquant_11_JOB_ID:$cuffquant_12_JOB_ID:$cuffquant_13_JOB_ID:$cuffquant_14_JOB_ID:$cuffquant_15_JOB_ID
JOB_DONE=job_output/cuffdiff/cuffdiff.16-cell_vs_8-cell.63221dd2ddfc3c49e1e7133eb977fa69.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.16-cell_vs_8-cell.63221dd2ddfc3c49e1e7133eb977fa69.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/16-cell_vs_8-cell && \
cuffdiff -u \
  --frag-bias-correct /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/16-cell_vs_8-cell \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/8-cell_rep1/abundances.cxb,cufflinks/8-cell_rep2/abundances.cxb,cufflinks/8-cell_rep3/abundances.cxb \
  cufflinks/16-cell_rep1/abundances.cxb,cufflinks/16-cell_rep2/abundances.cxb,cufflinks/16-cell_rep3/abundances.cxb
cuffdiff.16-cell_vs_8-cell.63221dd2ddfc3c49e1e7133eb977fa69.mugqic.done
)
cuffdiff_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffdiff_5_JOB_ID: cuffdiff.Blastocyst_vs_16-cell
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.Blastocyst_vs_16-cell
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID:$cuffquant_13_JOB_ID:$cuffquant_14_JOB_ID:$cuffquant_15_JOB_ID:$cuffquant_16_JOB_ID:$cuffquant_17_JOB_ID:$cuffquant_18_JOB_ID
JOB_DONE=job_output/cuffdiff/cuffdiff.Blastocyst_vs_16-cell.0a8c2e217623a1c7cba1623c799d9bdc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.Blastocyst_vs_16-cell.0a8c2e217623a1c7cba1623c799d9bdc.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/Blastocyst_vs_16-cell && \
cuffdiff -u \
  --frag-bias-correct /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/Blastocyst_vs_16-cell \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/16-cell_rep1/abundances.cxb,cufflinks/16-cell_rep2/abundances.cxb,cufflinks/16-cell_rep3/abundances.cxb \
  cufflinks/Blastocyst_rep1/abundances.cxb,cufflinks/Blastocyst_rep2/abundances.cxb,cufflinks/Blastocyst_rep3/abundances.cxb
cuffdiff.Blastocyst_vs_16-cell.0a8c2e217623a1c7cba1623c799d9bdc.mugqic.done
)
cuffdiff_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffdiff_6_JOB_ID: cuffdiff.4-cell_vs_GV
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.4-cell_vs_GV
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID:$cuffquant_1_JOB_ID:$cuffquant_2_JOB_ID:$cuffquant_3_JOB_ID:$cuffquant_7_JOB_ID:$cuffquant_8_JOB_ID:$cuffquant_9_JOB_ID
JOB_DONE=job_output/cuffdiff/cuffdiff.4-cell_vs_GV.030335ab37ece6c3f323d8239646eb02.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.4-cell_vs_GV.030335ab37ece6c3f323d8239646eb02.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/4-cell_vs_GV && \
cuffdiff -u \
  --frag-bias-correct /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/4-cell_vs_GV \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/GV-oocyte_rep1/abundances.cxb,cufflinks/GV-oocyte_rep2/abundances.cxb,cufflinks/GV-oocyte_rep3/abundances.cxb \
  cufflinks/4-cell_rep1/abundances.cxb,cufflinks/4-cell_rep2/abundances.cxb,cufflinks/4-cell_rep3/abundances.cxb
cuffdiff.4-cell_vs_GV.030335ab37ece6c3f323d8239646eb02.mugqic.done
)
cuffdiff_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffdiff_7_JOB_ID: cuffdiff.8-cell_vs_GV
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.8-cell_vs_GV
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID:$cuffquant_1_JOB_ID:$cuffquant_2_JOB_ID:$cuffquant_3_JOB_ID:$cuffquant_10_JOB_ID:$cuffquant_11_JOB_ID:$cuffquant_12_JOB_ID
JOB_DONE=job_output/cuffdiff/cuffdiff.8-cell_vs_GV.548c58015803f4ec7f398167fc8ab8d3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.8-cell_vs_GV.548c58015803f4ec7f398167fc8ab8d3.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/8-cell_vs_GV && \
cuffdiff -u \
  --frag-bias-correct /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/8-cell_vs_GV \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/GV-oocyte_rep1/abundances.cxb,cufflinks/GV-oocyte_rep2/abundances.cxb,cufflinks/GV-oocyte_rep3/abundances.cxb \
  cufflinks/8-cell_rep1/abundances.cxb,cufflinks/8-cell_rep2/abundances.cxb,cufflinks/8-cell_rep3/abundances.cxb
cuffdiff.8-cell_vs_GV.548c58015803f4ec7f398167fc8ab8d3.mugqic.done
)
cuffdiff_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffdiff_8_JOB_ID: cuffdiff.16-cell_vs_GV
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.16-cell_vs_GV
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID:$cuffquant_1_JOB_ID:$cuffquant_2_JOB_ID:$cuffquant_3_JOB_ID:$cuffquant_13_JOB_ID:$cuffquant_14_JOB_ID:$cuffquant_15_JOB_ID
JOB_DONE=job_output/cuffdiff/cuffdiff.16-cell_vs_GV.7d446daf9e78b1305da07483bf009929.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.16-cell_vs_GV.7d446daf9e78b1305da07483bf009929.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/16-cell_vs_GV && \
cuffdiff -u \
  --frag-bias-correct /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/16-cell_vs_GV \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/GV-oocyte_rep1/abundances.cxb,cufflinks/GV-oocyte_rep2/abundances.cxb,cufflinks/GV-oocyte_rep3/abundances.cxb \
  cufflinks/16-cell_rep1/abundances.cxb,cufflinks/16-cell_rep2/abundances.cxb,cufflinks/16-cell_rep3/abundances.cxb
cuffdiff.16-cell_vs_GV.7d446daf9e78b1305da07483bf009929.mugqic.done
)
cuffdiff_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffdiff_9_JOB_ID: cuffdiff.Blastocyst_vs_GV
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.Blastocyst_vs_GV
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID:$cuffquant_1_JOB_ID:$cuffquant_2_JOB_ID:$cuffquant_3_JOB_ID:$cuffquant_16_JOB_ID:$cuffquant_17_JOB_ID:$cuffquant_18_JOB_ID
JOB_DONE=job_output/cuffdiff/cuffdiff.Blastocyst_vs_GV.6a1879b4c69007ddee7145cc9342d904.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.Blastocyst_vs_GV.6a1879b4c69007ddee7145cc9342d904.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/Blastocyst_vs_GV && \
cuffdiff -u \
  --frag-bias-correct /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/Blastocyst_vs_GV \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/GV-oocyte_rep1/abundances.cxb,cufflinks/GV-oocyte_rep2/abundances.cxb,cufflinks/GV-oocyte_rep3/abundances.cxb \
  cufflinks/Blastocyst_rep1/abundances.cxb,cufflinks/Blastocyst_rep2/abundances.cxb,cufflinks/Blastocyst_rep3/abundances.cxb
cuffdiff.Blastocyst_vs_GV.6a1879b4c69007ddee7145cc9342d904.mugqic.done
)
cuffdiff_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffdiff_10_JOB_ID: cuffdiff.Blastocyst_vs_8-cell
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.Blastocyst_vs_8-cell
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID:$cuffquant_10_JOB_ID:$cuffquant_11_JOB_ID:$cuffquant_12_JOB_ID:$cuffquant_16_JOB_ID:$cuffquant_17_JOB_ID:$cuffquant_18_JOB_ID
JOB_DONE=job_output/cuffdiff/cuffdiff.Blastocyst_vs_8-cell.fd44d48ce4bd678429ad1c0e7f6dfbc9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.Blastocyst_vs_8-cell.fd44d48ce4bd678429ad1c0e7f6dfbc9.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/Blastocyst_vs_8-cell && \
cuffdiff -u \
  --frag-bias-correct /project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/genome/Bos_taurus.UMD3.1.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/Blastocyst_vs_8-cell \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/8-cell_rep1/abundances.cxb,cufflinks/8-cell_rep2/abundances.cxb,cufflinks/8-cell_rep3/abundances.cxb \
  cufflinks/Blastocyst_rep1/abundances.cxb,cufflinks/Blastocyst_rep2/abundances.cxb,cufflinks/Blastocyst_rep3/abundances.cxb
cuffdiff.Blastocyst_vs_8-cell.fd44d48ce4bd678429ad1c0e7f6dfbc9.mugqic.done
)
cuffdiff_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: cuffnorm
#-------------------------------------------------------------------------------
STEP=cuffnorm
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: cuffnorm_1_JOB_ID: cuffnorm
#-------------------------------------------------------------------------------
JOB_NAME=cuffnorm
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID:$cuffquant_1_JOB_ID:$cuffquant_2_JOB_ID:$cuffquant_3_JOB_ID:$cuffquant_4_JOB_ID:$cuffquant_5_JOB_ID:$cuffquant_6_JOB_ID:$cuffquant_7_JOB_ID:$cuffquant_8_JOB_ID:$cuffquant_9_JOB_ID:$cuffquant_10_JOB_ID:$cuffquant_11_JOB_ID:$cuffquant_12_JOB_ID:$cuffquant_13_JOB_ID:$cuffquant_14_JOB_ID:$cuffquant_15_JOB_ID:$cuffquant_16_JOB_ID:$cuffquant_17_JOB_ID:$cuffquant_18_JOB_ID
JOB_DONE=job_output/cuffnorm/cuffnorm.4598462939826fdfd6b11c4a03ccfcba.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffnorm.4598462939826fdfd6b11c4a03ccfcba.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffnorm && \
cuffnorm -q  \
  --library-type fr-firststrand \
  --output-dir cuffnorm \
  --num-threads 8 \
  --labels GV-oocyte_rep1,GV-oocyte_rep2,GV-oocyte_rep3,MII-oocyte_rep1,MII-oocyte_rep2,MII-oocyte_rep3,4-cell_rep1,4-cell_rep2,4-cell_rep3,8-cell_rep1,8-cell_rep2,8-cell_rep3,16-cell_rep1,16-cell_rep2,16-cell_rep3,Blastocyst_rep1,Blastocyst_rep2,Blastocyst_rep3 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/GV-oocyte_rep1/abundances.cxb \
  cufflinks/GV-oocyte_rep2/abundances.cxb \
  cufflinks/GV-oocyte_rep3/abundances.cxb \
  cufflinks/MII-oocyte_rep1/abundances.cxb \
  cufflinks/MII-oocyte_rep2/abundances.cxb \
  cufflinks/MII-oocyte_rep3/abundances.cxb \
  cufflinks/4-cell_rep1/abundances.cxb \
  cufflinks/4-cell_rep2/abundances.cxb \
  cufflinks/4-cell_rep3/abundances.cxb \
  cufflinks/8-cell_rep1/abundances.cxb \
  cufflinks/8-cell_rep2/abundances.cxb \
  cufflinks/8-cell_rep3/abundances.cxb \
  cufflinks/16-cell_rep1/abundances.cxb \
  cufflinks/16-cell_rep2/abundances.cxb \
  cufflinks/16-cell_rep3/abundances.cxb \
  cufflinks/Blastocyst_rep1/abundances.cxb \
  cufflinks/Blastocyst_rep2/abundances.cxb \
  cufflinks/Blastocyst_rep3/abundances.cxb
cuffnorm.4598462939826fdfd6b11c4a03ccfcba.mugqic.done
)
cuffnorm_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffnorm\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"cuffnorm\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$cuffnorm_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: fpkm_correlation_matrix
#-------------------------------------------------------------------------------
STEP=fpkm_correlation_matrix
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: fpkm_correlation_matrix_1_JOB_ID: fpkm_correlation_matrix_transcript
#-------------------------------------------------------------------------------
JOB_NAME=fpkm_correlation_matrix_transcript
JOB_DEPENDENCIES=$cuffnorm_1_JOB_ID
JOB_DONE=job_output/fpkm_correlation_matrix/fpkm_correlation_matrix_transcript.dd36f12d31d4efce73062b0bf461e872.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'fpkm_correlation_matrix_transcript.dd36f12d31d4efce73062b0bf461e872.mugqic.done'
module load mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics && \
R --no-save --no-restore <<-EOF
dataFile=read.table("cuffnorm/isoforms.fpkm_table",header=T,check.names=F)
fpkm=cbind(dataFile[,2:ncol(dataFile)])
corTable=cor(log2(fpkm+0.1))
corTableOut=rbind(c('Vs.',colnames(corTable)),cbind(rownames(corTable),round(corTable,3)))
write.table(corTableOut,file="metrics/transcripts_fpkm_correlation_matrix.tsv",col.names=F,row.names=F,sep="	",quote=F)
print("done.")

EOF
fpkm_correlation_matrix_transcript.dd36f12d31d4efce73062b0bf461e872.mugqic.done
)
fpkm_correlation_matrix_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"fpkm_correlation_matrix\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"fpkm_correlation_matrix\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$fpkm_correlation_matrix_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: fpkm_correlation_matrix_2_JOB_ID: fpkm_correlation_matrix_gene
#-------------------------------------------------------------------------------
JOB_NAME=fpkm_correlation_matrix_gene
JOB_DEPENDENCIES=$cuffnorm_1_JOB_ID
JOB_DONE=job_output/fpkm_correlation_matrix/fpkm_correlation_matrix_gene.60f3f6a5d19daa981493a76f44f36b5e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'fpkm_correlation_matrix_gene.60f3f6a5d19daa981493a76f44f36b5e.mugqic.done'
module load mugqic/R_Bioconductor/3.4.2_3.6 && \
R --no-save --no-restore <<-EOF
dataFile=read.table("cuffnorm/genes.fpkm_table",header=T,check.names=F)
fpkm=cbind(dataFile[,2:ncol(dataFile)])
corTable=cor(log2(fpkm+0.1))
corTableOut=rbind(c('Vs.',colnames(corTable)),cbind(rownames(corTable),round(corTable,3)))
write.table(corTableOut,file="metrics/gene_fpkm_correlation_matrix.tsv",col.names=F,row.names=F,sep="	",quote=F)
print("done.")

EOF
fpkm_correlation_matrix_gene.60f3f6a5d19daa981493a76f44f36b5e.mugqic.done
)
fpkm_correlation_matrix_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"fpkm_correlation_matrix\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"fpkm_correlation_matrix\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$fpkm_correlation_matrix_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: gq_seq_utils_exploratory_analysis_rnaseq
#-------------------------------------------------------------------------------
STEP=gq_seq_utils_exploratory_analysis_rnaseq
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: gq_seq_utils_exploratory_analysis_rnaseq_1_JOB_ID: gq_seq_utils_exploratory_analysis_rnaseq
#-------------------------------------------------------------------------------
JOB_NAME=gq_seq_utils_exploratory_analysis_rnaseq
JOB_DEPENDENCIES=$raw_counts_metrics_1_JOB_ID:$cuffnorm_1_JOB_ID
JOB_DONE=job_output/gq_seq_utils_exploratory_analysis_rnaseq/gq_seq_utils_exploratory_analysis_rnaseq.b49d43ce9914f0fad2d8ead70d4ba7af.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'gq_seq_utils_exploratory_analysis_rnaseq.b49d43ce9914f0fad2d8ead70d4ba7af.mugqic.done'
module load mugqic/R_Bioconductor/3.4.2_3.6 mugqic/mugqic_R_packages/1.0.5 && \
mkdir -p exploratory && \
R --no-save --no-restore <<-EOF
suppressPackageStartupMessages(library(gqSeqUtils))

exploratoryAnalysisRNAseq(htseq.counts.path="DGE/rawCountMatrix.csv", cuffnorm.fpkms.dir="cuffnorm", genes.path="/project/6007512/C3G/analyste_dev/genomes/species/Bos_taurus.UMD3.1/annotations/Bos_taurus.UMD3.1.Ensembl84.genes.tsv", output.dir="exploratory")
desc = readRDS(file.path("exploratory","index.RData"))
write.table(desc,file=file.path("exploratory","index.tsv"),sep='	',quote=F,col.names=T,row.names=F)
print("done.")

EOF
gq_seq_utils_exploratory_analysis_rnaseq.b49d43ce9914f0fad2d8ead70d4ba7af.mugqic.done
)
gq_seq_utils_exploratory_analysis_rnaseq_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"gq_seq_utils_exploratory_analysis_rnaseq\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"gq_seq_utils_exploratory_analysis_rnaseq\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=00:30:0 --mem=4G -N 1 -n 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$gq_seq_utils_exploratory_analysis_rnaseq_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: gq_seq_utils_exploratory_analysis_rnaseq_2_JOB_ID: gq_seq_utils_exploratory_analysis_rnaseq_report
#-------------------------------------------------------------------------------
JOB_NAME=gq_seq_utils_exploratory_analysis_rnaseq_report
JOB_DEPENDENCIES=$gq_seq_utils_exploratory_analysis_rnaseq_1_JOB_ID
JOB_DONE=job_output/gq_seq_utils_exploratory_analysis_rnaseq/gq_seq_utils_exploratory_analysis_rnaseq_report.29278144299c5c599281811b9a248725.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'gq_seq_utils_exploratory_analysis_rnaseq_report.29278144299c5c599281811b9a248725.mugqic.done'
module load mugqic/R_Bioconductor/3.4.2_3.6 mugqic/pandoc/1.15.2 && \
R --no-save --no-restore <<-'EOF'
report_dir="report";
input_rmarkdown_file = '/home/efournie/genpipes/bfx/report/RnaSeq.gq_seq_utils_exploratory_analysis_rnaseq.Rmd'
render_output_dir    = 'report'
rmarkdown_file       = basename(input_rmarkdown_file) # honoring a different WD that location of Rmd file in knitr is problematic
file.copy(from = input_rmarkdown_file, to = rmarkdown_file, overwrite = T)
rmarkdown::render(input = rmarkdown_file, output_format = c("html_document","md_document"), output_dir = render_output_dir  )
file.remove(rmarkdown_file)
EOF
gq_seq_utils_exploratory_analysis_rnaseq_report.29278144299c5c599281811b9a248725.mugqic.done
)
gq_seq_utils_exploratory_analysis_rnaseq_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$gq_seq_utils_exploratory_analysis_rnaseq_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: gq_seq_utils_exploratory_analysis_rnaseq_3_JOB_ID: cuffnorm_report
#-------------------------------------------------------------------------------
JOB_NAME=cuffnorm_report
JOB_DEPENDENCIES=$cuffmerge_1_JOB_ID
JOB_DONE=job_output/gq_seq_utils_exploratory_analysis_rnaseq/cuffnorm_report.59777e2c95a1c5c6e936bd8d1f54682f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffnorm_report.59777e2c95a1c5c6e936bd8d1f54682f.mugqic.done'
mkdir -p report && \
zip -r report/cuffAnalysis.zip cufflinks/ cuffdiff/ cuffnorm/ && \
cp \
  /home/efournie/genpipes/bfx/report/RnaSeq.cuffnorm.md \
  report/RnaSeq.cuffnorm.md
cuffnorm_report.59777e2c95a1c5c6e936bd8d1f54682f.mugqic.done
)
gq_seq_utils_exploratory_analysis_rnaseq_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$gq_seq_utils_exploratory_analysis_rnaseq_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: differential_expression
#-------------------------------------------------------------------------------
STEP=differential_expression
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: differential_expression_1_JOB_ID: differential_expression
#-------------------------------------------------------------------------------
JOB_NAME=differential_expression
JOB_DEPENDENCIES=$raw_counts_metrics_1_JOB_ID
JOB_DONE=job_output/differential_expression/differential_expression.c4f6a0baac3ce3a06446b0339fab650d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'differential_expression.c4f6a0baac3ce3a06446b0339fab650d.mugqic.done'
module load mugqic/mugqic_tools/2.1.9 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p DGE && \
Rscript $R_TOOLS/edger.R \
  -d ../../raw/design.txt \
  -c DGE/rawCountMatrix.csv \
  -o DGE && \
Rscript $R_TOOLS/deseq.R \
  -d ../../raw/design.txt \
  -c DGE/rawCountMatrix.csv \
  -o DGE \
  
differential_expression.c4f6a0baac3ce3a06446b0339fab650d.mugqic.done
)
differential_expression_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"differential_expression\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,/cvmfs/soft.mugqic/CentOS6/genomes/species/Bos_taurus.UMD3.1/Bos_taurus.UMD3.1.ini\" \
  -s \"differential_expression\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/GV-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/MII-oocyte_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/4-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/8-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/16-cell_rep3.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep1.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep2.json,/project/6009125/GSE52415_mito/output/pipeline/json/Blastocyst_rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=10:00:0 --mem=4G -N 1 -n 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$differential_expression_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# Call home with pipeline statistics
#-------------------------------------------------------------------------------
wget "http://mugqic.hpc.mcgill.ca/cgi-bin/pipeline.cgi?hostname=cedar5.cedar.computecanada.ca&ip=206.12.124.6&pipeline=RnaSeq&steps=picard_sam_to_fastq,trimmomatic,merge_trimmomatic_stats,star,picard_merge_sam_files,picard_sort_sam,picard_mark_duplicates,picard_rna_metrics,estimate_ribosomal_rna,bam_hard_clip,rnaseqc,wiggle,raw_counts,raw_counts_metrics,cufflinks,cuffmerge,cuffquant,cuffdiff,cuffnorm,fpkm_correlation_matrix,gq_seq_utils_exploratory_analysis_rnaseq,differential_expression&samples=18&AnonymizedList=fda2d58f64366e494255f45eb18f1441,b76b9af5cbbca537501cccd16055285a,9d272bc6ebd0423fc4d0d15b4c1024d7,2f91b3d0556177695f3f85b9888511f3,f2eff749f28ca915092de9e3051ca1bc,7cb976bcbcc1ccab6b64d9e9a210e3d8,655a5c242e3967176b811f7185780284,7360187969c93806a267e56dd209606a,1a48788c3ab277ab397010ab9ce8ba99,9a4b7f65e02be383b65f0463a38a3317,df624bbe8bf5a1e4391ee3c39d170c72,e64d7a281b8875af076908f01f6da7db,c81742201d7fc56ce1528d0a079bb896,4d35a963455b95afe36716f40c5f2adf,8f416d0b07d259d6c747ea5e1e720375,8ee5ad00fd1664e3c5cc4ea4743f2bd4,a8c0f717cb505f49a94896b3fd36b22d,5352a4dd4761281f6d1eb8bdeb46f00a" --quiet --output-document=/dev/null

